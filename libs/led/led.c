/**
 * @file led.c
 * @author Roni Kreinin
 * @date October 1, 2018
 * @brief LED library
 */
 
 #include "led.h"

/* Pattern definitions */
static uint16_t off_pattern[6]   = {0, 0, 0, 0, 0, 0};
static uint16_t on_pattern[6]    = {1, 0, 0, 0, 0, 0};
static uint16_t slow_pattern[6]  = {BSLOW_ON_CNT, BSLOW_OFF_CNT, 0, 0, 0, 0};
static uint16_t std_pattern[6]   = {BSTD_ON_CNT, BSTD_OFF_CNT, 0, 0, 0, 0};
static uint16_t b2_pattern[6]    = {B2_ON1_CNT, B2_OFF1_CNT, B2_ON2_CNT, B2_OFF2_CNT, 0, 0};
static uint16_t flash_pattern[6] = {BFLASH_ON_CNT, BFLASH_OFF_CNT, 0, 0, 0, 0};

uint8_t numLed;
static bool initialized;
static led_lib_pattern_t ledPatterns[LED_PATTERN_CNT] = {0};
static uint8_t ledId = 0;
static uint16_t freq;


/* Array of LEDs */
static led_lib_t * hLed;

/* Function prototypes */
void LED_Lib_Initialize_Patterns   ( void );
void LED_Lib_Pattern_Set           ( led_lib_mode_t mode, uint16_t * pattern );
void LED_Lib_On                    ( uint8_t id, uint8_t ch );
void LED_Lib_Off                   ( uint8_t id, uint8_t ch );
 

/**
 * @brief  Set the patterns for each LED mode
 * @param  mode : LED mode 
 * @param  pattern : Pointer to blink pattern associated with mode
 * @retval None
 */
void LED_Lib_Pattern_Set ( led_lib_mode_t mode, uint16_t * pattern )
{
  uint8_t ii  = 0;

  ledPatterns[mode].patternLen = 0;

  for ( ii = 0; ii < LED_MAX_PATTERN_LEN; ii++ )
  {
    if ( pattern[ii] != 0 )
    {
      ledPatterns[mode].patternLen++;
    }
    ledPatterns[mode].pattern[ii] = pattern[ii];
  }
}

 /**
  * @brief  Initialize Patterns
  * @param  None
  * @retval None
  */
void LED_Lib_Initialize_Patterns ( void )
{
  LED_Lib_Pattern_Set( LED_OFF,        off_pattern );
  LED_Lib_Pattern_Set( LED_ON,         on_pattern );
  LED_Lib_Pattern_Set( LED_BLINK_SLOW, slow_pattern );
  LED_Lib_Pattern_Set( LED_BLINK_STD,  std_pattern );
  LED_Lib_Pattern_Set( LED_BLINK_2,    b2_pattern );
  LED_Lib_Pattern_Set( LED_FLASH,      flash_pattern );
}
  

/**
  * @brief  Set the LED mode 
  * @param  id : LED ID
  * @param  mode : Blink mode 
  * @param  ch : Blink active channel
  * @retval None
  */
void LED_Lib_Mode_Set ( uint8_t id, led_lib_mode_t mode, uint8_t ch )
{
  hLed[id].ch            = ch;
  hLed[id].mode          = mode;
  hLed[id].counter       = ledPatterns[mode].pattern[0];
  hLed[id].counter      *= freq;
  hLed[id].counter      /= 1000;
  hLed[id].patternIndex  = 0;
  
  /* Set initial state */
  if (mode == LED_OFF)
  {
    LED_Lib_Off( id, ch );
  }
  else
  {
    LED_Lib_On( id, ch );
  }
}

 /**
  * @brief   Get the mode of an LED
  * @param   id : LED ID
  * @retval  Mode
  */
led_lib_mode_t LED_Lib_Mode_Get ( uint8_t id )
{
  return hLed[id].mode;
}

 /**
  * @brief   Get the channel of an LED
  * @param   id : LED ID
  * @retval  channel
  */
uint8_t LED_Lib_Active_Channel_Get ( uint8_t id )
{
  return hLed[id].ch;
}

 /**
  * @brief   Called by the LED updating timer to update LED state
  * @details Increments LED on/off counters depending on blink pattern and sets state accordingly
  * @param   None
  * @retval  None
  */
void LED_Lib_Timer_Handler ( void )
{
  if (initialized == false)
  {
    return;
  }
  
  uint8_t ledEnabledCounter = 0;
  uint8_t ii = 0;

  /* Update all LEDs */
  for (ii = 0; ii < numLed; ii++)
  {
    if ( hLed[ii].enabled )
    {
      ledEnabledCounter++;
      if ( hLed[ii].mode != LED_OFF && hLed[ii].mode != LED_ON )
      {
        if ( hLed[ii].counter > 0 )
        {
          hLed[ii].counter--;
        }
        /* If counter has reached zero, toggle the LED state and set counter to next */
        else
        {
          hLed[ii].patternIndex++;
          if ( hLed[ii].patternIndex == ledPatterns[hLed[ii].mode].patternLen )
          {
            /* Last counter has run down. Start over */
            hLed[ii].patternIndex = 0;
            LED_Lib_On( ii, hLed[ii].ch );
          }
          else
          {
            /* Toggle the LED */
            if (hLed[ii].state == true)
            {
              LED_Lib_Off( ii, hLed[ii].ch );
            }
            else
            {
              LED_Lib_On( ii, hLed[ii].ch );
            }
          }
          /* Update next count down value */
          hLed[ii].counter = ledPatterns[hLed[ii].mode].pattern[hLed[ii].patternIndex] * freq;
          hLed[ii].counter /= 1000;
        }
      }
    }
  }
}
 
 /**
  * @brief  Turn on an LED
  * @param  id : LED ID
  * @param  ch : Which channel to turn on
  * @retval None
  */
void LED_Lib_On ( uint8_t id, uint8_t ch )
{
  if (hLed[id].enabled)
  {
    hLed[id].led_set_cb( true, ch );
    hLed[id].state = true;
  }
}

 /**
  * @brief  Turn off an LED
  * @param  id : LED ID
  * @param  ch : Which channel to turn off
  * @retval None
  */
void LED_Lib_Off ( uint8_t id, uint8_t ch )
{
  if (hLed[id].enabled)
  {
    hLed[id].led_set_cb( false, ch );
    hLed[id].state = false;
  }
}

 /**
  * @brief  Toggle an LEDs state
  * @param  id : LED ID
  * @param  ch : Which channel to toggle
  * @retval None
  */
void LED_Lib_Toggle_State ( uint8_t id, uint8_t ch )
{
  if (hLed[id].enabled)
  {
    if (hLed[id].mode == LED_OFF)
    {
      LED_Lib_Mode_Set( id, LED_ON, ch );
    }
    else
    {
      LED_Lib_Mode_Set( id, LED_OFF, ch );
    }
  }
}

 /**
  * @brief  Toggle an LED between two channels
  * @param  id : LED ID
  * @param  ch1, ch2 : channels to toggle
  * @retval None
  */
void LED_Lib_Toggle_Channel ( uint8_t id, uint8_t ch1, uint8_t ch2 )
{
  if (hLed[id].enabled)
  {
    /* If the state of the current channel is off, turn on the other channel */
    if ( hLed[id].ch == ch1 && hLed[id].state == false )
    {
      LED_Lib_Mode_Set( id, LED_ON, ch2 );
    }
    else if ( hLed[id].ch == ch2 && hLed[id].state == false )
    {
      LED_Lib_Mode_Set( id, LED_ON, ch1 );
    }
     /* If the state of the current channel is on, turn off current channel, then turn on the other channel */
    else if ( hLed[id].ch == ch1 && hLed[id].state == true )
    {
      LED_Lib_Mode_Set( id, LED_OFF, ch1 );
      LED_Lib_Mode_Set( id, LED_ON, ch2 );
    }
    else if ( hLed[id].ch == ch2 && hLed[id].state == true )
    {
      LED_Lib_Mode_Set( id, LED_OFF, ch2 );
      LED_Lib_Mode_Set( id, LED_ON, ch1 );
    }
  }
}

 /**
  * @brief  Add an LED
  * @param  init : Initialization struct
  * @retval ID of the LED
  */
int8_t LED_Lib_Add ( led_set_cb_t cbFunc )
{
  hLed[ledId].state         = false;
  hLed[ledId].enabled       = true;
  hLed[ledId].counter       = 0;
  hLed[ledId].patternIndex  = 0;
  hLed[ledId].mode          = LED_OFF;
  hLed[ledId].led_set_cb    = cbFunc;
  hLed[ledId].ch            = 0;

  return ledId++;
}

 /**
  * @brief  Remove an LED
  * @param  id : LED ID
  * @retval Error value
  */
led_lib_err_t LED_Lib_Remove ( uint8_t id )
{
  if ( id >= numLed )
  {
    return LED_ERR_OUT_OF_RANGE;
  }
  hLed[id].state   = false;
  hLed[id].enabled = false;

  return LED_ERR_SUCCESS;
}

 /**
  * @brief  Initialize LED Library
  * @param  init : Initialization struct
  * @retval Error value
  */
led_lib_err_t LED_Lib_Initialize ( led_lib_init_t init )
{
  /* Initialize Patterns */
  LED_Lib_Initialize_Patterns();
  
  numLed = init.numLed;
  freq = init.frequency;
  
  hLed = malloc( numLed * sizeof(led_lib_t) );
  
  if (hLed == NULL)
  {
    return LED_ERR_MALLOC_NULL;
  }
  
  initialized = true;
  return LED_ERR_SUCCESS;
}
/**
  * @brief  Deinitialize LED Library
  * @param  None
  * @retval None
  */
void LED_Lib_Deinitialize ( void )
{
  ledId = 0;
  initialized = false;
  free( hLed );
}
