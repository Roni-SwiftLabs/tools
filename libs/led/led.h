/**
 * @file led.h
 * @author Roni Kreinin
 * @date October 1, 2018
 * @brief LED library
 */
#ifndef __LED_H
#define __LED_H

#include <stdint.h>
#include <stdbool.h>
#include <stdlib.h>

/* Max length of a blink pattern is 6 states */
#define LED_MAX_PATTERN_LEN   6

/* LED blink pattern timing definitions - 50ms increments only!!! */
#define BSLOW_ON_MS           350    /* On  Time */
#define BSLOW_OFF_MS          1650   /* Off Time */

#define BSTD_ON_MS            350    /* On  Time */
#define BSTD_OFF_MS           650    /* Off Time */

#define B2_ON1_MS             200    /* On  Time */
#define B2_OFF1_MS            100    /* Off Time */
#define B2_ON2_MS             200    /* On  Time */
#define B2_OFF2_MS            500    /* Off Time */

#define BFLASH_ON_MS          150    /* On  Time */
#define BFLASH_OFF_MS         150    /* Off Time */

#define BSLOW_ON_CNT          BSLOW_ON_MS  
#define BSLOW_OFF_CNT         BSLOW_OFF_MS 
#define BSTD_ON_CNT           BSTD_ON_MS   
#define BSTD_OFF_CNT          BSTD_OFF_MS  
#define B2_ON1_CNT            B2_ON1_MS    
#define B2_OFF1_CNT           B2_OFF1_MS   
#define B2_ON2_CNT            B2_ON2_MS    
#define B2_OFF2_CNT           B2_OFF2_MS   
#define BFLASH_ON_CNT         BFLASH_ON_MS 
#define BFLASH_OFF_CNT        BFLASH_OFF_MS


/* Callback typedefs */
typedef void ( *led_set_cb_t ) ( bool set, uint8_t ch ); /**< Callback typedef to led enable/disable function */


/**
 * @brief LED blink patterns
 */
typedef enum
{
  LED_OFF = 0x00,         /**< LED persist off */
  LED_ON,                 /**< LED persist on */
  LED_BLINK_SLOW,         /**< Slow blink */
  LED_BLINK_STD,          /**< Standard blink */
  LED_BLINK_2,            /**< Double blink */
  LED_FLASH,              /**< LED Flash */
  LED_PATTERN_CNT,
} led_lib_mode_t;

/**
 * @brief General error typedef
 */
typedef enum
{
  LED_ERR_SUCCESS,           /**< Successful initialization */
  LED_ERR_ALREADY_EXISTS,    /**< Attempting to add an LED that already exists */
  LED_ERR_NOT_INITIALIZED,   /**< Attempting to enable an LED before initializing the library */
  LED_ERR_OUT_OF_RANGE,      /**< Attempting to disable an LED with an ID that is out of range */
  LED_ERR_MALLOC_NULL        /**< Malloc returns NULL */
} led_lib_err_t;

/**
 * @brief LED library init struct
 */
typedef struct
{
  uint8_t               numLed;     /**< Max number of LEDs*/
  uint16_t              frequency;  /**< Frequency at which the LED timer timeout occurs*/
} led_lib_init_t;


/**
 * @brief LED struct
 */
typedef struct
{
  bool                  state;         /**< Indicates if the LED is on or off */
  bool                  enabled;       /**< Indicates if the LED is enabled */
  uint32_t              counter;       /**< Counter used for pattern durations */
  uint16_t              patternIndex;  /**< Current pattern index */
  led_lib_mode_t        mode;          /**< Blink Mode */
  uint8_t               ch;            /**< Current channel of LED*/
  led_set_cb_t          led_set_cb;    /**< Callback to LED power setting function */
} led_lib_t;

/**
 * @brief LED pattern struct 
 */
typedef struct 
{
  uint8_t               patternLen;                     /**< Number of states in blink pattern */
  uint16_t              pattern[LED_MAX_PATTERN_LEN];   /**< Blink pattern state times */
} led_lib_pattern_t;

/* Function prototypes */
led_lib_err_t        LED_Lib_Initialize         ( led_lib_init_t init );
void                 LED_Lib_Deinitialize       ( void );
int8_t               LED_Lib_Add                ( led_set_cb_t cbFunc );
led_lib_err_t        LED_Lib_Remove             ( uint8_t id );
void                 LED_Lib_Timer_Handler      ( void );
void                 LED_Lib_Mode_Set           ( uint8_t id, led_lib_mode_t mode, uint8_t ch );
led_lib_mode_t       LED_Lib_Mode_Get           ( uint8_t id );
void                 LED_Lib_Toggle_State       ( uint8_t id, uint8_t ch );
void                 LED_Lib_Toggle_Channel     ( uint8_t id, uint8_t ch1, uint8_t ch2 );
uint8_t              LED_Lib_Active_Channel_Get ( uint8_t id );

#endif /* __LED_H */
