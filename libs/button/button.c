#include "button.h"

/* Callback functions */ 
timer_access_cb_t  timer_access_cb;
button_read_cb_t   button_read_cb;
button_event_cb_t  buttonCb;

/* Local variables */
static uint16_t    numBtn;
static bool        initialized;

/* Duration of events */
static       uint16_t eventDuration  [BUTTON_NUM_EVENTS];
static const uint16_t defaultDuration[BUTTON_NUM_EVENTS] = { DEFAULT_SHORT_DUR, DEFAULT_MED_DUR, DEFAULT_LONG_DUR} ;
static       uint8_t  debounceTicks;

/* Array of buttons */
button_t * hButton;


/**
  * @brief  Call when a button pressed event has occurred
  * @param  None
  * @retval None
  */
void Button_Pressed( void )
{
  /* Start button timer */
  timer_access_cb( true );
}


/**
  * @brief  Trigger a button event callback to the application if enables
  * @param  id The button id
  * @param  evt Event that was triggered
  * @retval None
  */
static void Button_Trigger_Event ( uint8_t id, button_evt_t evt )
{
  /* If both button and event are enabled trigger a callback */
  if ( hButton[id].enabled && hButton[id].eventEnabled[evt] )
  {
    buttonCb(id, evt);
  }
}


/**
  * @brief  Counts how long a button has been pressed, calls button callback with appropriate event
  * @param  None
  * @retval None
  */
void Button_Timer_Expired( void )
{
  uint8_t ii;
  uint8_t btnPressedCounter = 0;

  /* Iterate over buttons */
  for (ii = 0; ii < numBtn; ii ++)
  {
    /* Button is being held */
    if ( button_read_cb(ii) )
    {
      btnPressedCounter++;

      /* Long Press Condition : trigger right away */
      if ( hButton[ii].count == eventDuration[BUTTON_EVENT_LONG] )
      {
        hButton[ii].count = BTN_MAX_CNT;
        Button_Trigger_Event(ii, BUTTON_EVENT_LONG);
      }
      /* If we haven't reached the MAX count keep incrementing */
      else if ( hButton[ii].count < BTN_MAX_CNT )
      {
        hButton[ii].count++;
      }
    }

    /* Button has been released */
    else
    {
      if ( hButton[ii].count != BTN_MAX_CNT )
      {
        if (hButton[ii].count >= debounceTicks)
        {
          /* Short Press Condition */
          if ( hButton[ii].count < eventDuration[BUTTON_EVENT_SHORT] )
          {
            Button_Trigger_Event(ii, BUTTON_EVENT_SHORT);
          }
          /* Medium Press Condition */
          else if ( hButton[ii].count < eventDuration[BUTTON_EVENT_MED] )
          {
            Button_Trigger_Event(ii, BUTTON_EVENT_MED);
          }
        }
      }

      /* Clear counter once button is released */
      hButton[ii].count = 0;
    }
  }
  
  /* If all buttons have been released, stop the timer */
  if ( btnPressedCounter == 0 )
  {
     timer_access_cb( false );
  }
}


/**
  * @brief  Set the duration required for an event to occur, default if duration_ms == 0
  * @param  event - Event type, duration_ms - duration required for event in ms
  * @retval None
  */
static void Set_Event_Duration( button_evt_t event, uint16_t duration_ms, uint32_t freq )
{
  if (duration_ms == 0)
  {
    eventDuration[event] = defaultDuration[event];
  }
  else
  {
    uint32_t temp = ( duration_ms * freq ) / 1000;
    eventDuration[event] = temp;
  }
}


/**
  * @brief  Add an event to a button
  * @param  id - Id number, event - Event type
  * @retval Error value indicating the success of the Add
  */
button_err_t Button_Event_Enable ( uint8_t id, button_evt_t event )
{
  if ( initialized )
  {  
     if ( id >= numBtn )
    {
      return BUTTON_ERR_OUT_OF_RANGE;
    }

    if ( hButton[id].enabled == true )
    {
      hButton[id].eventEnabled[event] = true;
      return BUTTON_ERR_SUCCESS;
    }
    else
    {
      return BUTTON_ERR_NOT_ENABLED;
    }
  }
  else
  {
    return BUTTON_ERR_NOT_INITIALIZED;
  }
}


/**
  * @brief  Enable a button
  * @param  id The buttons id
  * @retval Error value indicating the success of the Add
  */
button_err_t Button_Enable ( uint8_t id )
{
  if ( initialized ) //Button_Initialize must have been called before adding buttons
  {
    if (hButton[id].enabled == false) //Check that the button isn't already enabled
    {
      hButton[id].enabled = true;
      hButton[id].count = 0;
      return BUTTON_ERR_SUCCESS;
    }
    return BUTTON_ERR_ALREADY_EXISTS;
  }
  return BUTTON_ERR_NOT_INITIALIZED;
}


/**
  * @brief  Remove a button
  * @param  id - Id number
  * @retval None
  */
button_err_t Button_Disable ( uint8_t id )
{
  if ( id >= numBtn )
  {
    return BUTTON_ERR_OUT_OF_RANGE;
  }

  hButton[id].enabled = false;
  return BUTTON_ERR_SUCCESS;
}


/**
  * @brief  Deinitialize buttons, button callbacks, and timer callbacks
  * @param  None
  * @retval None
  */
void Button_Deinitialize( void )
{
  initialized = false;
  
  for (int ii = 0; ii < numBtn; ii ++)
  {
    hButton[ii].enabled = false;
    hButton[ii].count = 0;
  }

  free(hButton);
}


/**
  * @brief  Initialize button and timer callbacks
  * @param  buttonInit - button_init_t with all the necessary callbacks and parameters
  * @retval None
  */
button_init_err_t Button_Initialize( button_init_t buttonInit ) 
{
  
  /* Initialize callbacks */
  buttonCb = buttonInit.buttonCb;
  timer_access_cb = buttonInit.timer_access_cb;
  button_read_cb = buttonInit.button_read_cb;

  /* Initialize parameters */
  numBtn = buttonInit.numBtn;

  /* Allocate memory for arrays */
  hButton = malloc(numBtn * sizeof(button_t));

  if (hButton == NULL)
  {
    return BUTTON_INIT_MALLOC_NULL;
  }

  /* Set event durations */
  Set_Event_Duration( BUTTON_EVENT_SHORT, buttonInit.short_dur, buttonInit.freq );
  Set_Event_Duration( BUTTON_EVENT_MED,   buttonInit.med_dur,   buttonInit.freq );
  Set_Event_Duration( BUTTON_EVENT_LONG,  buttonInit.long_dur,  buttonInit.freq );
  
  
  debounceTicks = ( ( BTN_DEBOUNCE_MS * buttonInit.freq ) / 1000 ) + 1;

  initialized = true;
  return BUTTON_INIT_SUCCESS;
}
