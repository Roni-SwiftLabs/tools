#include "button.h"
#include "timeout.h"

#define MS_TO_US(ms)       (ms * 1000)  

#define TIMEOUT_PERIOD1    1000
#define TIMEOUT_PERIOD2    2000
#define TIMEOUT_PERIOD3    1500
#define TIMEOUT_FREQ       100
#define BUTTON_FREQ        1000
#define TIMEOUT_PER_US     1000000/TIMEOUT_FREQ
#define BUTTON_PER_US      1000000/BUTTON_FREQ

#define MAX_PINS           10

#define BTN1               0
#define BTN2               1
#define BTN3               2

typedef struct 
{
  bool     isPressed;
  uint32_t turnOffTime;
} button_sim_t;

