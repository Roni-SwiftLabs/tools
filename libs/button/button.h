#ifndef __BUTTON_H
#define __BUTTON_H

#ifdef __cplusplus
 extern "C" {
#endif

#include <stdint.h>
#include <stdbool.h>
#include <stdlib.h>

#define BTN_MAX_CNT         3000
#define BTN_DEBOUNCE_MS     20
#define DEFAULT_SHORT_DUR   500
#define DEFAULT_MED_DUR     3000
#define DEFAULT_LONG_DUR    3000


/* Event type */
typedef enum 
{
  BUTTON_EVENT_SHORT,         /**< Short button press*/
  BUTTON_EVENT_MED,           /**< Medium button press*/
  BUTTON_EVENT_LONG,          /**< Long button press*/
  BUTTON_NUM_EVENTS           /**< Event type count*/
} button_evt_t;

/* Button error type */
typedef enum 
{
  BUTTON_ERR_SUCCESS,          /**< No error*/
  BUTTON_ERR_ALREADY_EXISTS,   /**< Already initialized*/
  BUTTON_ERR_NOT_INITIALIZED,  /**< Attempting to add event on uninitialized button*/
  BUTTON_ERR_NOT_ENABLED,
  BUTTON_ERR_INIT_ERR,
  BUTTON_ERR_OUT_OF_RANGE
} button_err_t;

/* Initialization error type */
typedef enum
{
  BUTTON_INIT_SUCCESS,
  BUTTON_INIT_MALLOC_NULL
} button_init_err_t;

/* Callback typedefs */
typedef void ( *button_event_cb_t )         ( uint8_t id, button_evt_t event );  /**< Callback typedef to event handler function*/
typedef void ( *timer_access_cb_t )         ( bool enable );                      /**< Callback typedef to timer start/stop function*/
typedef bool ( *button_read_cb_t )          ( uint8_t id );                       /**< Callback typedef to button status function*/


/* Button struct */
typedef struct 
{
  uint32_t  count;                              /**< Counter representing how long the button has been held for*/
  bool      enabled;                            /**< Boolean representing if the button is enabled*/
  bool      longTriggered;                      /**< Tracks if long button event has already been reported */
  bool      eventEnabled[BUTTON_NUM_EVENTS];    /**< Boolean array representing which events are enabled*/
} button_t;


/* Button initialization struct */
typedef struct 
{
  button_event_cb_t   buttonCb;         /**< Event handler function*/
  timer_access_cb_t   timer_access_cb;  /**< Timer start/stop function*/
  button_read_cb_t    button_read_cb;   /**< Button status function*/
  uint32_t            freq;             /**< Frequency at which Button_Timer_Expired is run*/
  uint16_t            short_dur;        /**< Duration of short press. If set to 0, default value is usused*/
  uint16_t            long_dur;         /**< Duration of long press. If set to 0, default value is unused*/
  uint16_t            med_dur;          /**< Duration of medium press. If set to 0, default value is unused*/
  uint16_t            numBtn;           /**< Max number of buttons being used*/
} button_init_t;


/* Exported functions */
void              Button_Timer_Expired     ( void );
void              Button_Pressed           ( void );
button_err_t      Button_Event_Enable      ( uint8_t id, button_evt_t event );
button_err_t      Button_Enable            ( uint8_t id );
button_err_t      Button_Disable           ( uint8_t id );
button_init_err_t Button_Initialize        ( button_init_t buttonInit );
void              Button_Deinitialize      ( void );

#ifdef __cplusplus
}
#endif

#endif
