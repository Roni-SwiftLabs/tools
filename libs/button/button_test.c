#include "button_test.h"
#include "time.h"

#include <stdbool.h>
#include <stdlib.h>
#include <stdio.h>
#include <unistd.h>
#include <pthread.h>


void Button_Event_Handler       ( uint8_t btn, button_evt_t event );
void Check_Buttons              ( uint64_t tNow );
void *timerThread               ( void *vargp );
static uint32_t get_usec        ( struct timespec t );

/* Simulated buttons */
button_sim_t button[MAX_PINS];

/* Event results returned by buttons */
button_evt_t eventResults[MAX_PINS] = { BUTTON_NUM_EVENTS };

uint32_t t0_us; 

/* Timer Enabled flag */
bool timerEn = false;

/**
  * @brief  Disable timer
  * @param  None
  * @retval None
  */
void Timer_Stop ( void )
{
  timerEn = false;
}

/**
  * @brief  Enable timer
  * @param  None
  * @retval None
  */
void Timer_Start ( void )
{
  timerEn = true;
}

/**
  * @brief  Used as callback to enable/disable timer
  * @param  enable - true = start timer, false = stop timer
  * @retval None
  */
void Timer_Callback ( bool enable )
{
  if ( enable )
  {
    Timer_Start();  
  }
  else
  {
    Timer_Stop();  
  }
}

/**
  * @brief  Clear test results
  * @param  None
  * @retval None
  */
void Reset_Test_Results( void )
{
  for (int ii = 0; ii < MAX_PINS; ii++)
  {
    eventResults[ii] = BUTTON_NUM_EVENTS;
  }
}

/**
  * @brief  Callback used to handle events, saves results to eventResults
  * @param  id - Button ID, event - Event type
  * @retval None
  */
void Button_Event_Handler ( uint8_t id, button_evt_t event)
{
  eventResults[id] = event;
}

/**
  * @brief  Timer thread used for the button module and for simulating button presses
  * @param  None
  * @retval None
  */
void *timerThread( void *vargp )
{
  struct timespec t0, tNow;
  uint32_t tNextTick_us, tNextBtnTick_us, tNow_us;

  clock_gettime(CLOCK_MONOTONIC, &t0);
  t0_us = get_usec(t0);

  tNextTick_us = t0_us + TIMEOUT_PER_US;
  tNextBtnTick_us = t0_us + BUTTON_PER_US;

  while (1)
  {
    clock_gettime(CLOCK_MONOTONIC, &tNow);
    tNow_us = get_usec(tNow);

    /* 1kHz tick */
    if ( tNow_us >= tNextTick_us )
    {
      tNextTick_us += TIMEOUT_PER_US;
      Check_Buttons( tNow_us );
    }

    /* Button module tick */
    if ( tNow_us >= tNextBtnTick_us )
    {
      tNextBtnTick_us += BUTTON_PER_US;
      if ( timerEn )
      {
        Button_Timer_Expired();
      }
    }
  }
}

/**
  * @brief  Callback used to return status of a button
  * @param  id - Button ID
  * @retval bool - true = button is pressed, false = button is not pressed
  */
bool Button_Is_Pressed ( uint8_t id )
{
  return button[id].isPressed;
}

/**
  * @brief  Check whether enough time has elapsed for 'release' a button
  * @param  tNow - current time in microseconds
  * @retval None
  */
void Check_Buttons ( uint64_t tNow )
{
  for (int ii = 0; ii < MAX_PINS; ii ++)
  {
    if ( button[ii].isPressed && tNow >= button[ii].turnOffTime )
    {
      button[ii].isPressed = false;
    }
  }
}

/**
  * @brief  Simulate button press
  * @param  id - Button ID, duration_ms - Duration for which the button is held, in ms
  * @retval None
  */
void Simulate_Button_Push ( uint8_t id, uint16_t duration_ms )
{
  struct timespec tInit;
  clock_gettime(CLOCK_MONOTONIC, &tInit);
  button[id].isPressed = true;
  button[id].turnOffTime = get_usec(tInit) + MS_TO_US(duration_ms);
  Button_Pressed();
}

/**
  * @brief  Convert timespec to microseconds
  * @param  t - timespec
  * @retval time in microseconds
  */
static uint32_t get_usec(struct timespec t)
{
  return (t.tv_sec * 1000000) + (t.tv_nsec / 1000 );
}

/**
  * @brief  Test a short press
  * @param  None
  * @retval bool - true = test passed, false = test failed
  */
bool Simulation_Test_Short ( void )
{
  bool result = true;
  uint16_t short_dur = 700;

  button_init_t buttonInit = 
  {
    .buttonCb = &Button_Event_Handler,
    .timer_access_cb = &Timer_Callback,
    .button_read_cb = &Button_Is_Pressed,
    .freq = BUTTON_FREQ,
    .short_dur = short_dur,
    .med_dur = 0,
    .long_dur = 0,
    .numBtn = MAX_PINS
  };

  if ( Button_Initialize( buttonInit ) != BUTTON_INIT_SUCCESS ) 
  {
    result = false;
  }
  
  usleep(10000);

  if ( Button_Enable( BTN1 ) == BUTTON_ERR_SUCCESS )
  {
    Button_Event_Enable( BTN1, BUTTON_EVENT_SHORT );
  }
  else
  {
    result = false;
  }

  Simulate_Button_Push( BTN1, short_dur / 2 );

  usleep( 2 * MS_TO_US( short_dur) );

  if (eventResults[BTN1] != BUTTON_EVENT_SHORT)
  {
    result = false;
  }

  Button_Deinitialize();
  Reset_Test_Results();
  return result;
}

/**
  * @brief  Test a medium press
  * @param  None
  * @retval bool - true = test passed, false = test failed
  */
bool Simulation_Test_Med ( void )
{
  bool result = true;
  uint16_t med_dur = 2000;
  uint16_t long_dur = 3000;
  button_init_t buttonInit = 
  {
    .buttonCb = &Button_Event_Handler,
    .timer_access_cb = &Timer_Callback,
    .button_read_cb = &Button_Is_Pressed,
    .freq = BUTTON_FREQ,
    .short_dur = 0,
    .med_dur = med_dur,
    .long_dur = long_dur,
    .numBtn = MAX_PINS
  };

  if ( Button_Initialize( buttonInit ) != BUTTON_INIT_SUCCESS )
  {
    result = false;
  } 

  usleep(10000);

  if ( Button_Enable( BTN1 ) == BUTTON_ERR_SUCCESS )
  {
    Button_Event_Enable( BTN1, BUTTON_EVENT_MED );
  }
  else
  {
    result = false;
  }

  Simulate_Button_Push( BTN1, med_dur / 2 );
  usleep( 2 * MS_TO_US(med_dur) );
  if (eventResults[BTN1] != BUTTON_EVENT_MED)
  {
    printf("%d\r\n", eventResults[BTN1]);
    result = false;
  }

  Button_Deinitialize();
  Reset_Test_Results();
  return result;
}

/**
  * @brief  Test a long press
  * @param  None
  * @retval bool - true = test passed, false = test failed
  */
bool Simulation_Test_Long ( void )
{
  bool result = true;
  uint16_t long_dur = 3000;
  button_init_t buttonInit = 
  {
    .buttonCb = &Button_Event_Handler,
    .timer_access_cb = &Timer_Callback,
    .button_read_cb = &Button_Is_Pressed,
    .freq = BUTTON_FREQ,
    .short_dur = 0,
    .med_dur = 0,
    .long_dur = long_dur,
    .numBtn = MAX_PINS
  };

  if ( Button_Initialize( buttonInit ) != BUTTON_INIT_SUCCESS ) 
  {
    result = false;
  }
  usleep(10000);

  if ( Button_Enable( BTN1 ) == BUTTON_ERR_SUCCESS )
  {
    Button_Event_Enable( BTN1, BUTTON_EVENT_LONG );
  }
  else
  {
    result = false;
  }

  Simulate_Button_Push( BTN1, long_dur );
  usleep( 2 * MS_TO_US( long_dur ) );
  if (eventResults[BTN1] != BUTTON_EVENT_LONG)
  {
    result = false;
  }

  Button_Deinitialize();
  Reset_Test_Results();
  return result;
}

/**
  * @brief  Test a press longer than the long duration
  * @param  None
  * @retval bool - true = test passed, false = test failed
  */
bool Simulation_Test_Longer ( void )
{
  bool result = true;
  uint16_t long_dur = 3000;
  uint16_t longer_dur = 4000;
  button_init_t buttonInit = 
  {
    .buttonCb = &Button_Event_Handler,
    .timer_access_cb = &Timer_Callback,
    .button_read_cb = &Button_Is_Pressed,
    .freq = BUTTON_FREQ,
    .short_dur = 0,
    .med_dur = 0,
    .long_dur = long_dur,
    .numBtn = MAX_PINS
  };

  if ( Button_Initialize( buttonInit ) != BUTTON_INIT_SUCCESS ) 
  {
    result = false;
  }
  usleep(10000);

  if ( Button_Enable( BTN1 ) == BUTTON_ERR_SUCCESS )
  {
    Button_Event_Enable( BTN1, BUTTON_EVENT_LONG );
  }
  else
  {
    result = false;
  }
  Simulate_Button_Push( BTN1, longer_dur );
  usleep( 2 * MS_TO_US(longer_dur) );
  if (eventResults[BTN1] != BUTTON_EVENT_LONG)
  {
    result = false;
  }

  Button_Deinitialize();
  Reset_Test_Results();
  return result;
}


/**
  * @brief  Test adding a button without initializing first
  * @param  None
  * @retval bool - true = test passed, false = test failed
  */
bool Simulation_Test_Add_Button_Uninitialized ( void )
{
  bool result = true;
  if ( Button_Enable( BTN1 ) != BUTTON_ERR_NOT_INITIALIZED )
  {
    result = false;
  }

  Button_Deinitialize();
  Reset_Test_Results();
  return result;
}

/**
  * @brief  Test adding an event without initializing a button first
  * @param  None
  * @retval bool - true = test passed, false = test failed
  */
bool Simulation_Test_Add_Event_Uninitialized ( void )
{
  bool result = true;
  if ( Button_Event_Enable( BTN1, BUTTON_EVENT_LONG ) != BUTTON_ERR_NOT_INITIALIZED )
  {
    result = false;
    printf("1\r\n");
  }

  button_init_t buttonInit = 
  {
    .buttonCb = &Button_Event_Handler,
    .timer_access_cb = &Timer_Callback,
    .button_read_cb = &Button_Is_Pressed,
    .freq = BUTTON_FREQ,
    .short_dur = 0,
    .med_dur = 0,
    .long_dur = 0,
    .numBtn = MAX_PINS
  };

  Button_Initialize( buttonInit );
  
  if ( Button_Event_Enable( BTN1, BUTTON_EVENT_LONG ) != BUTTON_ERR_NOT_ENABLED )
  {
    result = false;
    printf("2\r\n");
  }
  Button_Deinitialize();
  Reset_Test_Results();
  return result;
}

/**
  * @brief  Test adding the same button twice
  * @param  None
  * @retval bool - true = test passed, false = test failed
  */
bool Simulation_Test_Add_Twice ( void )
{
  bool result = true;
  button_init_t buttonInit = 
  {
    .buttonCb = &Button_Event_Handler,
    .timer_access_cb = &Timer_Callback,
    .button_read_cb = &Button_Is_Pressed,
    .freq = BUTTON_FREQ,
    .short_dur = 0,
    .med_dur = 0,
    .long_dur = 0,
    .numBtn = MAX_PINS
  };

  Button_Initialize( buttonInit );
  
  if ( Button_Enable( BTN1 ) == BUTTON_ERR_SUCCESS )
  {
    if ( Button_Enable( BTN1 ) != BUTTON_ERR_ALREADY_EXISTS )
    {
      result = false;
    }
  }
  else
  {
    result = false;
  }
  Button_Deinitialize();
  Reset_Test_Results();
  return result;
}

/**
  * @brief  Test deleting a button
  * @param  None
  * @retval bool - true = test passed, false = test failed
  */
bool Simulation_Test_Del ( void )
{
  bool result = true;

  button_init_t buttonInit = 
  {
    .buttonCb = &Button_Event_Handler,
    .timer_access_cb = &Timer_Callback,
    .button_read_cb = &Button_Is_Pressed,
    .freq = BUTTON_FREQ,
    .short_dur = 0,
    .med_dur = 0,
    .long_dur = 0,
    .numBtn = MAX_PINS
  };

  if ( Button_Initialize( buttonInit ) != BUTTON_INIT_SUCCESS ) 
  {
    result = false;
  }  
  usleep(10000);

  if ( Button_Enable( BTN1 ) == BUTTON_ERR_SUCCESS )
  {
    Button_Disable( BTN1 );
  }

  if (Button_Event_Enable( BTN1, BUTTON_EVENT_LONG ) != BUTTON_ERR_NOT_ENABLED)
  {
    result = false;
  }

  Button_Deinitialize();
  Reset_Test_Results();
  return result;
}

/**
  * @brief  Test pushing two buttons simultaneously 
  * @param  None
  * @retval bool - true = test passed, false = test failed
  */
bool Simulation_Test_Two_Buttons ( void )
{
  bool result = true;
  uint16_t long_dur = 3000;
  button_init_t buttonInit = 
  {
    .buttonCb = &Button_Event_Handler,
    .timer_access_cb = &Timer_Callback,
    .button_read_cb = &Button_Is_Pressed,
    .freq = BUTTON_FREQ,
    .short_dur = 0,
    .med_dur = 0,
    .long_dur = long_dur,
    .numBtn = MAX_PINS
  };

  if ( Button_Initialize( buttonInit ) != BUTTON_INIT_SUCCESS ) 
  {
    result = false;
  }

  usleep(10000);

  if ( Button_Enable( BTN1 ) == BUTTON_ERR_SUCCESS && Button_Enable( BTN2 ) == BUTTON_ERR_SUCCESS )
  {
    Button_Event_Enable( BTN1, BUTTON_EVENT_LONG );
    Button_Event_Enable( BTN2, BUTTON_EVENT_LONG );
  }
  else
  {
    result = false;
  }

  Simulate_Button_Push( BTN1, long_dur );
  Simulate_Button_Push( BTN2, long_dur );
  usleep( 2 * MS_TO_US( long_dur ) );
  if ( eventResults[BTN1] != BUTTON_EVENT_LONG || eventResults[BTN2] != BUTTON_EVENT_LONG )
  {
    result = false;
  }

  Button_Deinitialize();
  Reset_Test_Results();
  return result;
}

int main (int argc, char *argv[])
{
  /* Start timer thread */
  pthread_t timer_id;
  pthread_create( &timer_id, NULL, timerThread, NULL );
  
  /* Run tests and print results */
  printf("Short press test result:              %d\r\n", Simulation_Test_Short());
  printf("Medium press test result:             %d\r\n", Simulation_Test_Med());
  printf("Long press test result:               %d\r\n", Simulation_Test_Long());
  printf("Longer press test result:             %d\r\n", Simulation_Test_Longer());
  printf("Add button uninitialized test result: %d\r\n", Simulation_Test_Add_Button_Uninitialized());
  printf("Add event uninitialized test result:  %d\r\n", Simulation_Test_Add_Event_Uninitialized());
  printf("Add button twice test result:         %d\r\n", Simulation_Test_Add_Twice());
  printf("Delete button test result:            %d\r\n", Simulation_Test_Del());
  printf("Two buttons test result:              %d\r\n", Simulation_Test_Two_Buttons());
  printf("########## Testing Complete ###########\r\n");
  while(1);
  
  return 0;
}