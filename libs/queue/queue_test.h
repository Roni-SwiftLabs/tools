
/**
 * @file queue_test.h
 * @author Daniel Bujak
 * @date August 29, 2018
 * @brief Queue testing header
 */

/* Define to prevent recursive inclusion -------------------------------------*/
#ifndef __QUEUE_TEST_H
#define __QUEUE_TEST_H


#define NUM_TESTS 13

#ifndef bool 
  typedef int bool;
  #define true 1
  #define false 0
#endif

typedef enum{
  QUEUE_TEST_PASS,
  QUEUE_TEST_FAIL,
  QUEUE_TEST_PUSH_FAIL,
  QUEUE_TEST_PUSH_DATA_FAIL,
  QUEUE_TEST_POP_FAIL,
  QUEUE_TEST_POP_DATA_FAIL,
  QUEUE_TEST_DISCARD_FAIL,
  QUEUE_TEST_PEEK_FAIL,
  QUEUE_TEST_CLEAR_FAIL
}queue_test_result_t;

typedef enum{
  QUEUE_TEST,
  QUEUE_SUBTEST
}queue_test_t;

typedef enum{
  LOG_OFF = 0,
  LOG_ERROR,
  LOG_NORMAL,
  LOG_SUBTEST,
  LOG_VERBOSE,
  LOG_VVERBOSE
}log_level_t;


typedef queue_test_result_t (* queueTestFunc_t) ( size_t eSize, size_t capacity, uint16_t * testNum, bool circular );

bool Queue_Test_Main ( void );


#endif /* __QUEUE_TEST_H */
