/**
 * @file queue.h
 * @author Daniel Bujak
 * @date Feb 20, 2018
 * @brief Queue related definitions and function prototypes
 *
 */

/* Define to prevent recursive inclusion -------------------------------------*/
#ifndef __QUEUE_H
#define __QUEUE_H

#ifdef __cplusplus
 extern "C" {
#endif


#include <stddef.h>
#include <stdint.h>
#include <stdbool.h>


/**
 * Queue size is one larger than capacity to allow t = h condition to be empty queue
 * queueInitialize ( capacity = 3, size = 5, circular = false )
 *
 *   queue->dStart
 *   |
 * [ x, x, x, x, x     
     x, x, x, x, x 
     x, x, x, x, x 
     x, x, x, x, x ]  End of queue data buffer
     x
     |
     queue->dEnd

 * index  Init Push Push Push  Pop  Pop  Pop                                     
 *   0     t,h   h    h    h                                              
 *   1           t              h                                         
 *   2                t              h                                     
 *   3                     t    t    t   t,h                                 
 * Size     0    1    2    3    2    1    0                               
*/


/**
 * @brief Structure to represent a queue
 */
typedef struct seque
{
  bool          circular;             /**< Indicates if queue allows overwriting of oldest data */
  uint8_t       capacity;             /**< Max size of queue */
  size_t        dataSize;             /**< Size of data structure to be stored in queue */
  uint8_t *     head;                 /**< Index of head */
  uint8_t *     tail;                 /**< Index of tail */
  uint8_t *     dStart;               /**< Pointer to start of queue data region */
  uint8_t *     dEnd;                 /**< Pointer to end of queue data region */
} seque_t;


/**
 * @brief Queue error messages
 */
typedef enum
{
  QUEUE_OK,                           /**< Command successful */
  QUEUE_ERROR,                        /**< General error */
  QUEUE_EMPTY,                        /**< Queue is empty */
  QUEUE_FULL                          /**< Queue is full */
} queue_error_t;


/* Function Prototypes */
seque_t *     queueInitialize   ( uint8_t capacity, size_t dSize, bool circular );
void          queueDeInitialize ( seque_t * q );
bool          queueIsFull       ( seque_t * queue );
bool          queueIsEmpty      ( seque_t * queue );
bool          queueGetCircular  ( seque_t * queue );
uint8_t       queueGetCapacity  ( seque_t * queue );
uint8_t       queueGetSize      ( seque_t * queue );
size_t        queueGetDataSize  ( seque_t * queue );
queue_error_t queuePush         ( seque_t * queue, void * data, uint16_t len );
queue_error_t queuePop          ( seque_t * queue, void * item );
queue_error_t queueDiscard      ( seque_t * queue );
queue_error_t queueClear        ( seque_t * queue );
queue_error_t queueHead         ( seque_t * queue, void ** head );
queue_error_t queueTail         ( seque_t * queue, void ** tail );
void *        queueNextData     ( seque_t * queue, void * item );
void *        queuePrevData     ( seque_t * queue, void * item );


#ifdef __cplusplus
}
#endif
#endif /* __QUEUE_H */

