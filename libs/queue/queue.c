/* Includes ------------------------------------------------------------------*/
#include "queue.h"

#include <stdlib.h>
#include <string.h>



/**@brief Returns true if the queue is full. The queue is full when it's size
 *        is equal to it's capacity
 *
 * @param[in]  queue - pointer to the queue to check
 *
 * @return Whether or not the queue is full
 */
bool queueIsFull( seque_t * queue )
{
  return ( queueGetSize(queue) == queue->capacity );
}


/**@brief Returns true if the queue is empty. The queue is full when it's size is 0
 *
 * @param[in]  queue - pointer to the queue to check
 *
 * @return Whether or not the queue is empty
 */
bool queueIsEmpty(seque_t* queue)
{
  return ( queueGetSize(queue) == 0 );
}


/**@brief Returns true if the queue is circular. 
 *
 * @param[in]  queue - pointer to the queue to check
 *
 * @return Whether or not the queue is circular
 */
bool queueGetCircular ( seque_t * queue )
{
  return queue->circular;
}


/**@brief Returns the capacity of the queue
 *
 * @param[in]  queue - pointer to the queue to check
 *
 * @return Max number of entries in the queue
 */
uint8_t queueGetCapacity ( seque_t * queue )
{
  return queue->capacity;
}


/**@brief Returns the size of the queue
 *
 * @param[in]  queue - pointer to the queue to check
 *
 * @return Number of entries in the queue
 */
uint8_t queueGetSize ( seque_t* queue )
{
  uint16_t size;

  if ( queue->tail >= queue->head )
  {
    size = (queue->tail - queue->head) / queue->dataSize;
  }
  else
  {
    size = ( ( queue->tail + ( (queue->capacity + 1) * queue->dataSize) ) - queue->head ) / queue->dataSize;
  }

  return size;
}


/**@brief Returns the data size of the queue
 *
 * @param[in]  queue - pointer to the queue to check
 *
 * @return Number of entries in the queue
 */
size_t queueGetDataSize ( seque_t* queue )
{
  return queue->dataSize;
}


/**@brief Adds an item to the queue. Changes the tail and size of queue
 *
 * @param[in]  queue - pointer to the queue to push to
 * @param[in]  item  - pointer to start of data to push to queue
 * @param[in]  len   - number of bytes to copy
 *
 * @return None
 */
queue_error_t queuePush(seque_t * queue, void * data, uint16_t len)
{
  /* Check if queue is full */
  if (queueIsFull(queue))
  {
    /* Allow queue to wrap if circular */
    if ( queue->circular == true )
    {
      //TODO: Mutex needed for simultaneous queue pop
      queue->head = queueNextData(queue, queue->head);
    }
    else
    {
      return QUEUE_FULL;
    }
  }

  /* Make sure size of data being added is limited */
  if (len > queue->dataSize)
  {
    len = queue->dataSize;
  }

  /* Copy Data */
  memcpy(queue->tail, data, len);

  /* Increment tail counter */
  queue->tail = queueNextData(queue, queue->tail);

  return QUEUE_OK;
}


/**@brief Remove an item from the queue. Changes the head of queue
 *
 * @param[in]  queue - pointer to the queue to pop from
 *
 * @return The popped item
 */
queue_error_t queuePop(seque_t* queue, void * item)
{
  if (queueIsEmpty(queue))
  {
    return QUEUE_EMPTY;
  }

  /* Copy out the data */
  memcpy(item, queue->head, queue->dataSize);

  /* Increment head counter */
  queue->head = queueNextData(queue, queue->head);

  return QUEUE_OK;
}


/**@brief Function to discard the head of the queue without returning it
 *
 * @param[in]  queue - pointer to the queue to discard the head of
 *
 * @return Error code
 */
queue_error_t queueDiscard( seque_t * queue )
{
  if (queueIsEmpty(queue))
  {
      return QUEUE_EMPTY;
  }

  queue->head = queueNextData(queue, queue->head);

  return QUEUE_OK;
}


/**@brief Function to clear a queue
 *
 * @param[in]  queue - pointer to the queue to clear
 *
 * @return Error code
 */
queue_error_t queueClear ( seque_t * queue )
{
  queue->head     = queue->dStart;
  queue->tail     = queue->dStart;
  return QUEUE_OK;
}


/**@brief Function to return the next entry in the queue
 *
 * @param[in]  queue - pointer to the queue to get the head of
 * @param[out] item -  pointer to current entry
 *
 * @return Error code
 */
void * queueNextData ( seque_t * queue, void * item )
{
  uint8_t * next = (uint8_t *)item + queue->dataSize;
  if ( next >= queue->dEnd )
  {
    next = queue->dStart;
  }
  return next;
}


/**@brief Function to return the previous entry in the queue
 *
 * @param[in]  queue - pointer to the queue to get the head of
 * @param[out] item -  pointer to current entry
 *
 * @return Error code
 */
void * queuePrevData ( seque_t * queue, void * item )
{
  uint8_t * prev = (uint8_t *)item - queue->dataSize;
  if ( prev < queue->dStart )
  {
    prev = queue->dEnd - queue->dataSize;
  }
  return prev;
}


/**@brief Function to get the head pointer of a queue
 *
 * @param[in]  queue - pointer to the queue to get the head of
 * @param[out] head  - pointer to the head
 *
 * @return Error code
 */
queue_error_t queueHead ( seque_t * queue, void ** head )
{
  *head = queue->head;
  
  if ( queueIsEmpty(queue) )
  {
    return QUEUE_EMPTY;
  }
  
  return QUEUE_OK;
}


/**@brief Function to get the tail pointer of a queue
 *
 * @details Note: the tail actually points to the location where the next
 *          data should be written. Therefore we must return a pointer to 
 *          the previous entry
 *
 * @param[in]  queue - pointer to the queue to get the tail of
 * @param[out] tail  - pointer to the tail
 *
 * @return Error code
 */
queue_error_t queueTail ( seque_t * queue, void ** tail )
{
  /* When the queue is empty, return a pointer to the tail */
  if ( queueIsEmpty(queue) )
  {
    *tail = queue->tail;
    return QUEUE_EMPTY;
  }
  
  /* When queue is not empty, return pointer to the most recently pushed data */
  *tail = queuePrevData(queue, queue->tail);
  return QUEUE_OK;
}


/**@brief Initializes a queue of given capacity. Size of the queue is initialized as 0
 * @details Sets the read and write pointers to 0
 *
 * @param[in]  capacity - number of elements in the queue
 *
 * @return Pointer to the queue object
 */
seque_t * queueInitialize( uint8_t capacity, size_t dSize, bool circular )
{
  /* Calculate data buffer size */
  uint16_t dataSize = ( dSize * (capacity + 1) );

  /* Allocate queue + check return */
  seque_t * queue = malloc(sizeof(seque_t) + dataSize);
  if (queue == NULL)
  {
      return NULL;
  }

  queue->circular = circular;
  queue->capacity = capacity;
  queue->dataSize = dSize;
  queue->dStart   = (uint8_t *)queue + sizeof(seque_t);
  queue->dEnd     = queue->dStart + dataSize;
  queue->head     = queue->dStart;
  queue->tail     = queue->dStart;
  return queue;
}


/**@brief DeInitializes a queue. Frees the memory allocated for that queue
 * @param[in]  q - pointer to the queue to free
 */
void queueDeInitialize( seque_t * q )
{
  free(q);
}

