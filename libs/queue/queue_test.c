#include <stdio.h>
#include <stdint.h>
#include <stdlib.h>
#include <string.h>
#include <stdarg.h>

#include "queue.h"
#include "queue_test.h"

log_level_t systemLogLevel = LOG_NORMAL;

static queue_test_result_t Queue_Test_Seq_1 (size_t eSize, size_t capacity, uint16_t * testNum, bool circular);
static queue_test_result_t Queue_Test_Seq_2 (size_t eSize, size_t capacity, uint16_t * testNum, bool circular);
static queue_test_result_t Queue_Test_Seq_3 (size_t eSize, size_t capacity, uint16_t * testNum, bool circular);
static queue_test_result_t Queue_Test_Seq_4 (size_t eSize, size_t capacity, uint16_t * testNum, bool circular);
static queue_test_result_t Queue_Test_Seq_5 (size_t eSize, size_t capacity, uint16_t * testNum, bool circular);
static queue_test_result_t Queue_Test_Seq_6 (size_t eSize, size_t capacity, uint16_t * testNum, bool circular);
static queue_test_result_t Queue_Test_Seq_7 (size_t eSize, size_t capacity, uint16_t * testNum, bool circular);
static queue_test_result_t Queue_Test_Seq_8 (size_t eSize, size_t capacity, uint16_t * testNum, bool circular);
static queue_test_result_t Queue_Test_Seq_9 (size_t eSize, size_t capacity, uint16_t * testNum, bool circular);
static queue_test_result_t Queue_Test_Seq_10 (size_t eSize, size_t capacity, uint16_t * testNum, bool circular);
static queue_test_result_t Queue_Test_Seq_11 (size_t eSize, size_t capacity, uint16_t * testNum, bool circular);
static queue_test_result_t Queue_Test_Seq_12 (size_t eSize, size_t capacity, uint16_t * testNum, bool circular);
static queue_test_result_t Queue_Test_Seq_13 (size_t eSize, size_t capacity, uint16_t * testNum, bool circular);


queueTestFunc_t testFunctions[NUM_TESTS] = {
  Queue_Test_Seq_1,
  Queue_Test_Seq_2,
  Queue_Test_Seq_3,
  Queue_Test_Seq_4,
  Queue_Test_Seq_5,
  Queue_Test_Seq_6,
  Queue_Test_Seq_7,
  Queue_Test_Seq_8,
  Queue_Test_Seq_9,
  Queue_Test_Seq_10,
  Queue_Test_Seq_11,
  Queue_Test_Seq_12,
  Queue_Test_Seq_13
};

void Print( log_level_t log_level, char *format, ... )
{
  if ( log_level <= systemLogLevel )
  {
    va_list args;
    va_start(args, format);
    vprintf(format, args);
    va_end(args);
  }
}

uint8_t * Queue_Test_Generate_Data ( size_t eSize, size_t cap )
{
  uint16_t ii = 0;
  uint8_t * data = calloc(cap, eSize);
  uint8_t * pDest = data;  

  uint8_t cannedData[eSize * cap];
  uint8_t * pSource = cannedData;

  for ( ii = 0; ii < eSize * cap; ii++ )
  {
    cannedData[ii] = (uint8_t)ii;
  }

  for ( ii = 0; ii < cap; ii++ )
  {
    memcpy(pDest, pSource, eSize);
    pDest += eSize;
    pSource += eSize;
  }

  return data;
}

void Buffer_Print ( uint8_t * data, uint16_t len, char * hdr )
{
  uint16_t ii = 0;

  Print(LOG_VERBOSE,"%s: ", hdr);

  for ( ii = 0; ii < len; ii++ )
  {
    Print(LOG_VERBOSE,"%.2d, ", *data++);
  }

  Print(LOG_VERBOSE," END.\r\n");
}

void Queue_Test_Print ( seque_t * q )
{
  uint8_t ii = 0;
  char printStr[5];
  
  uint8_t * headPtr;
  uint8_t * tailPtr;
  uint8_t capacity, dataSize;
  bool circular;
  
  queueHead(q, (void**)&headPtr);
  queueTail(q, (void**)&tailPtr);
  capacity = queueGetCapacity(q);
  dataSize = queueGetDataSize(q);
  circular = queueGetCircular(q);
  
  Print(LOG_VERBOSE, "\r\nvvvvvvvvvvvvvvvvv QUEUE DATA vvvvvvvvvvvvvvvvvvvvvvv\r\n");
  Print(LOG_VERBOSE,"Queue:\r\n");
  Print(LOG_VERBOSE,"  Head = %d\r\n", headPtr);
  Print(LOG_VERBOSE,"  Tail = %d\r\n", tailPtr);
  Print(LOG_VERBOSE,"  Cap  = %d\r\n", capacity);
  Print(LOG_VERBOSE,"  Circ = %d\r\n", circular);
  Print(LOG_VERBOSE,"  dSiz = %zu\r\n", dataSize);

  uint8_t * iterator = headPtr;

  Print(LOG_VERBOSE,"          ------          \r\n");
  if ( !queueIsEmpty(q) )
  {
    while ( iterator != tailPtr && ii < queueGetCapacity(q)) 
    {
      sprintf(printStr, "D%d", ii);
      Buffer_Print(iterator, queueGetDataSize(q), printStr);
      iterator = queueNextData(q, iterator);
      ii++;
    } 
  }
  else
  {
    Print(LOG_VERBOSE,"Queue is empty \r\n");
  }
  Print(LOG_VERBOSE, "^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^\r\n\n");

}

queue_test_result_t Queue_Test_Push ( seque_t * q, size_t eSize, size_t capacity, uint8_t pNum)
{
  uint8_t * pData = Queue_Test_Generate_Data(eSize, capacity);
  uint8_t * compData = pData;
  uint8_t * tailPtr;
  uint8_t ii;
  uint8_t initSize = 0;
  
  for (ii = 0; ii < pNum; ii++ )
  {
    Buffer_Print(pData, eSize, "Push Data");
    
    initSize = queueGetSize(q);
    
    if (queuePush(q, pData, eSize) != QUEUE_OK)
    {
      return QUEUE_TEST_PUSH_FAIL;
    }
    
    //Check that the data at the newest tail matches what was pushed
    queueTail(q, (void**)&tailPtr);
    if (memcmp(compData, tailPtr, queueGetDataSize(q)) != 0)
    {
      return QUEUE_TEST_PUSH_DATA_FAIL;
    }
    
    //Check that the size was incremented correctly
    if ((queueGetCircular(q) && !queueIsFull(q)) || !queueGetCircular(q))
    {
      if (queueGetSize(q) != initSize + 1 )
      {
        return QUEUE_TEST_PUSH_DATA_FAIL;
      }
    }
    
    compData+= eSize;
    pData += eSize;
  }

  
  return QUEUE_TEST_PASS;
}

queue_test_result_t Queue_Test_Discard ( seque_t * q, uint8_t dNum)
{
  uint8_t ii = 0;
  uint8_t initSize = 0;
  for (ii = 0; ii < dNum ; ii++ )
  {
    initSize = queueGetSize(q);
    
    if (queueDiscard(q) != QUEUE_OK)
    {
      return QUEUE_TEST_DISCARD_FAIL;
    }
    //Check that size was decremented correctly
    if ((queueGetSize(q) + 1) != initSize)
    {
      return QUEUE_TEST_DISCARD_FAIL;
    }
  }
  return QUEUE_TEST_PASS;
}

queue_test_result_t Queue_Test_Pop ( seque_t * q,  size_t eSize, uint8_t pNum)
{
  uint8_t ii = 0;
  uint8_t initSize = 0;
  uint8_t * popData = malloc(sizeof(uint8_t));
  uint8_t * headPtr;
  for (ii = 0; ii < pNum; ii++)
  {
    initSize = queueGetSize(q);
    queueHead(q, (void**)&headPtr);
    
    if ( queuePop(q, popData) == QUEUE_EMPTY)
    {
      return QUEUE_TEST_POP_FAIL;
    }
    
    Buffer_Print(popData, eSize, "Popped Data");
    
    //Check that the popped data matches what was in the queue
    if (memcmp(headPtr, popData, queueGetDataSize(q)) != 0)
    {
      free(popData);
      return QUEUE_TEST_POP_DATA_FAIL;
    }
    //Check that size was decremented correctly
    if ((queueGetSize(q) + 1) != initSize)
    {
      free(popData);
      return QUEUE_TEST_POP_DATA_FAIL;
    }
  }
  free(popData);
  return QUEUE_TEST_PASS;
}

queue_test_result_t Queue_Test_Clear ( seque_t * q )
{
  uint8_t * tailPtr, * headPtr;
  
  if (queueClear(q) != QUEUE_OK)
  {
    return QUEUE_TEST_CLEAR_FAIL;
  }
  //Check that the queue is empty
  if ( !queueIsEmpty(q))
  {
    return QUEUE_TEST_CLEAR_FAIL;
  }

  queueTail(q, (void**)&tailPtr);
  queueHead(q, (void**)&headPtr);
  
  //Check that the head and tail point to the same location
  if (tailPtr != headPtr)
  {
    return QUEUE_TEST_CLEAR_FAIL;
  }
  return QUEUE_TEST_PASS;
}

queue_test_result_t Queue_Test_Result_Print(queue_test_result_t result, uint16_t  *idx, queue_test_t testType)
{
  char * resultString = "Failed\r\n";
  char * testTypeString = testType == QUEUE_TEST ? "Test" : "Sub-Test";
  switch(result)
  {
      case QUEUE_TEST_PASS:
        resultString = "Passed\r\n";
        break;
      case QUEUE_TEST_PUSH_FAIL:
        resultString = "Push Failed\r\n";
        break;
      case QUEUE_TEST_POP_FAIL:
        resultString = "Pop Failed\r\n";
        break;
      case QUEUE_TEST_PEEK_FAIL:
        resultString = "Peek Failed\r\n";
        break;
      case QUEUE_TEST_DISCARD_FAIL:
        resultString = "Discard Failed\r\n";
        break;
      default:
        break;
  }
  if (testType == QUEUE_TEST)
  {
    Print(LOG_NORMAL,"%s %d: %s \r\n", testTypeString, *idx, resultString);
  }
  else
  {
    Print(LOG_SUBTEST,"%s %d: %s \r\n", testTypeString, *idx, resultString);
  }
  if (testType == QUEUE_TEST)
  {
    printf ("-------------------------------------------------\r\n");
  }
  (*idx)++;
  return result;
}

//Push to capacity, discard 1, pop (capacity - 2), peek
static queue_test_result_t Queue_Test_Seq_1 (size_t eSize, size_t capacity, uint16_t * testNum, bool circular)
{
  /* Initialize small queue */
  seque_t * sQueue = queueInitialize(capacity, eSize, circular);
  queue_test_result_t testResult = QUEUE_TEST_PASS;
  //Run tests
  uint16_t subtestIdx = 1;
  uint16_t * subtestIdxPtr = &subtestIdx;
  Print(LOG_SUBTEST, "\r\n-------------------------------------------------\r\n");
  Print(LOG_SUBTEST, "Test %d: \r\n", *testNum);
  Queue_Test_Print(sQueue);
  
  if (Queue_Test_Result_Print(Queue_Test_Push(sQueue, eSize, capacity, capacity), subtestIdxPtr, QUEUE_SUBTEST) != QUEUE_TEST_PASS) //Push to capacity
  { 
    testResult = QUEUE_TEST_FAIL;
  }
  Queue_Test_Print(sQueue);
  
  if (Queue_Test_Result_Print(Queue_Test_Discard(sQueue, 1), subtestIdxPtr, QUEUE_SUBTEST)!= QUEUE_TEST_PASS) //Discard 1 entry
  { 
    testResult = QUEUE_TEST_FAIL;
  }
  Queue_Test_Print(sQueue);
  
  if (Queue_Test_Result_Print(Queue_Test_Pop(sQueue, eSize, capacity - 2), subtestIdxPtr, QUEUE_SUBTEST)!= QUEUE_TEST_PASS)//Pop (capacity - 1) entries
  { 
   testResult = QUEUE_TEST_FAIL;
  }
  
  Queue_Test_Print(sQueue);
 
  queueDeInitialize(sQueue);
  
  return testResult;
}

//Push to capacity 
static queue_test_result_t Queue_Test_Seq_2 (size_t eSize, size_t capacity, uint16_t * testNum, bool circular)
{
  /* Initialize small queue */
  seque_t * sQueue = queueInitialize(capacity, eSize, circular);
  queue_test_result_t testResult = QUEUE_TEST_PASS;
  //Run tests
  uint16_t subtestIdx = 1;
  uint16_t * subtestIdxPtr = &subtestIdx;
  Print(LOG_SUBTEST, "\r\n-------------------------------------------------\r\n");
  Print(LOG_SUBTEST, "Test %d: \r\n", *testNum);
  Queue_Test_Print(sQueue);
  
  if (Queue_Test_Result_Print(Queue_Test_Push(sQueue, eSize, capacity, capacity), subtestIdxPtr, QUEUE_SUBTEST) != QUEUE_TEST_PASS) //Push to capacity
  { 
    testResult = QUEUE_TEST_FAIL;
  }
  Queue_Test_Print(sQueue);
 
  queueDeInitialize(sQueue);
  
  return testResult;
}

//Pop empty 
static queue_test_result_t Queue_Test_Seq_3 (size_t eSize, size_t capacity, uint16_t * testNum, bool circular)
{
  /* Initialize small queue */
  seque_t * sQueue = queueInitialize(capacity, eSize, circular);
  queue_test_result_t testResult = QUEUE_TEST_PASS;
  //Run tests
  uint16_t subtestIdx = 1;
  uint16_t * subtestIdxPtr = &subtestIdx;
  Print(LOG_SUBTEST, "\r\n-------------------------------------------------\r\n");
  Print(LOG_SUBTEST, "Test %d: \r\n", *testNum);
  Queue_Test_Print(sQueue);
  
  if (Queue_Test_Result_Print(Queue_Test_Pop(sQueue, eSize, 1), subtestIdxPtr, QUEUE_SUBTEST) != QUEUE_TEST_POP_FAIL) //Pop one
    testResult = QUEUE_TEST_FAIL;
  
  Queue_Test_Print(sQueue);
 
  queueDeInitialize(sQueue);
  return testResult;
}

//Discard empty 
static queue_test_result_t Queue_Test_Seq_4 (size_t eSize, size_t capacity, uint16_t * testNum, bool circular)
{
  /* Initialize small queue */
  seque_t * sQueue = queueInitialize(capacity, eSize, circular);
  queue_test_result_t testResult = QUEUE_TEST_PASS;
  //Run tests
  uint16_t subtestIdx = 1;
  uint16_t * subtestIdxPtr = &subtestIdx;
  Print(LOG_SUBTEST, "\r\n-------------------------------------------------\r\n");
  Print(LOG_SUBTEST, "Test %d: \r\n", *testNum);
  Queue_Test_Print(sQueue);
  
  if (Queue_Test_Result_Print(Queue_Test_Discard(sQueue, 1), subtestIdxPtr, QUEUE_SUBTEST) != QUEUE_TEST_DISCARD_FAIL) //Discard one
  {
    testResult = QUEUE_TEST_FAIL;
  }
  Queue_Test_Print(sQueue);
 
  queueDeInitialize(sQueue);
  return testResult;
}

//Clear empty 
static queue_test_result_t Queue_Test_Seq_5 (size_t eSize, size_t capacity, uint16_t * testNum, bool circular)
{
  /* Initialize small queue */
  seque_t * sQueue = queueInitialize(capacity, eSize, circular);
  queue_test_result_t testResult = QUEUE_TEST_PASS;
  //Run tests
  uint16_t subtestIdx = 1;
  uint16_t * subtestIdxPtr = &subtestIdx;
  Print(LOG_SUBTEST, "\r\n-------------------------------------------------\r\n");
  Print(LOG_SUBTEST, "Test %d: \r\n", *testNum);
  Queue_Test_Print(sQueue);
  
  if (Queue_Test_Result_Print(Queue_Test_Clear(sQueue), subtestIdxPtr, QUEUE_SUBTEST) != QUEUE_TEST_PASS) //Clear queue
    testResult = QUEUE_TEST_FAIL;
  
  Queue_Test_Print(sQueue);
 
  queueDeInitialize(sQueue);
  return testResult;
}

//Push past capacity
static queue_test_result_t Queue_Test_Seq_6 (size_t eSize, size_t capacity, uint16_t * testNum, bool circular)
{
  /* Initialize small queue */
  seque_t * sQueue = queueInitialize(capacity, eSize, circular);
  queue_test_result_t testResult = QUEUE_TEST_PASS;
  //Run tests
  uint16_t subtestIdx = 1;
  uint16_t * subtestIdxPtr = &subtestIdx;
  Print(LOG_SUBTEST, "\r\n-------------------------------------------------\r\n");
  Print(LOG_SUBTEST, "Test %d: \r\n", *testNum);
  Queue_Test_Print(sQueue);
  
  if (Queue_Test_Result_Print(Queue_Test_Push(sQueue, eSize, capacity, capacity), subtestIdxPtr, QUEUE_SUBTEST) != QUEUE_TEST_PASS) //Push to capacity
  {
    testResult = QUEUE_TEST_FAIL;
  }
  Queue_Test_Print(sQueue);
  
  if (circular)
  {
    if (Queue_Test_Result_Print(Queue_Test_Push(sQueue, eSize, capacity, capacity), subtestIdxPtr, QUEUE_SUBTEST) != QUEUE_TEST_PASS) //Push past capacity
    {
      testResult = QUEUE_TEST_FAIL;
    }
  }
  else
  {
    if (Queue_Test_Result_Print(Queue_Test_Push(sQueue, eSize, capacity, capacity), subtestIdxPtr, QUEUE_SUBTEST) != QUEUE_TEST_PUSH_FAIL)
    {
      testResult = QUEUE_TEST_FAIL;
    }
  }
  Queue_Test_Print(sQueue);
 
  queueDeInitialize(sQueue);
  return testResult;
}

//Push to capacity, pop to empty
static queue_test_result_t Queue_Test_Seq_7 (size_t eSize, size_t capacity, uint16_t * testNum, bool circular)
{
  /* Initialize small queue */
  seque_t * sQueue = queueInitialize(capacity, eSize, circular);
  queue_test_result_t testResult = QUEUE_TEST_PASS;
  //Run tests
  uint16_t subtestIdx = 1;
  uint16_t * subtestIdxPtr = &subtestIdx;
  Print(LOG_SUBTEST, "\r\n-------------------------------------------------\r\n");
  Print(LOG_SUBTEST, "Test %d: \r\n", *testNum);
  Queue_Test_Print(sQueue);
  
  if (Queue_Test_Result_Print(Queue_Test_Push(sQueue, eSize, capacity, capacity), subtestIdxPtr, QUEUE_SUBTEST) != QUEUE_TEST_PASS) //Push to capacity
  {
    testResult = QUEUE_TEST_FAIL;
  }
   Queue_Test_Print(sQueue);
   
  if (Queue_Test_Result_Print(Queue_Test_Pop(sQueue, eSize, capacity), subtestIdxPtr, QUEUE_SUBTEST) != QUEUE_TEST_PASS) //Pop all
  {
    testResult = QUEUE_TEST_FAIL;
  } 
  Queue_Test_Print(sQueue);
 
  queueDeInitialize(sQueue);
  return testResult;
}

//Push past capacity, pop past empty
static queue_test_result_t Queue_Test_Seq_8 (size_t eSize, size_t capacity, uint16_t * testNum, bool circular)
{
  /* Initialize small queue */
  seque_t * sQueue = queueInitialize(capacity, eSize, circular);
  queue_test_result_t testResult = QUEUE_TEST_PASS;
  //Run tests
  uint16_t subtestIdx = 1;
  uint16_t * subtestIdxPtr = &subtestIdx;
  Print(LOG_SUBTEST, "\r\n-------------------------------------------------\r\n");
  Print(LOG_SUBTEST, "Test %d: \r\n", *testNum);
  Queue_Test_Print(sQueue);
  if (circular)
  {
    if (Queue_Test_Result_Print(Queue_Test_Push(sQueue, eSize, capacity, capacity), subtestIdxPtr, QUEUE_SUBTEST) != QUEUE_TEST_PASS) //Push to capacity
    { 
      testResult = QUEUE_TEST_FAIL;
    }
    if (Queue_Test_Result_Print(Queue_Test_Push(sQueue, eSize, capacity, 1), subtestIdxPtr, QUEUE_SUBTEST) != QUEUE_TEST_PASS) //Push past capacity
    {
      testResult = QUEUE_TEST_FAIL;
    }
    
    Queue_Test_Print(sQueue);
   
    if (Queue_Test_Result_Print(Queue_Test_Pop(sQueue, eSize, capacity + 1), subtestIdxPtr, QUEUE_SUBTEST) != QUEUE_TEST_POP_FAIL) //Pop all
    {
      testResult = QUEUE_TEST_FAIL;
    }
  }
  else
  {
    if (Queue_Test_Result_Print(Queue_Test_Push(sQueue, eSize, capacity, capacity), subtestIdxPtr, QUEUE_SUBTEST) != QUEUE_TEST_PASS) //Push to capacity
    {
      testResult = QUEUE_TEST_FAIL;
    }
    
    if (Queue_Test_Result_Print(Queue_Test_Push(sQueue, eSize, capacity, 1), subtestIdxPtr, QUEUE_SUBTEST) != QUEUE_TEST_PUSH_FAIL) //Push past capacity
    {
      testResult = QUEUE_TEST_FAIL;
    }
    
    Queue_Test_Print(sQueue);
   
    if (Queue_Test_Result_Print(Queue_Test_Pop(sQueue, eSize, capacity + 1), subtestIdxPtr, QUEUE_SUBTEST) != QUEUE_TEST_POP_FAIL) //Pop all
    {
      testResult = QUEUE_TEST_FAIL; 
    }
  }
    
  Queue_Test_Print(sQueue);
 
  queueDeInitialize(sQueue);
  return testResult;
}

//Push to capacity, discard past empty
static queue_test_result_t Queue_Test_Seq_9 (size_t eSize, size_t capacity, uint16_t * testNum, bool circular)
{
  /* Initialize small queue */
  seque_t * sQueue = queueInitialize(capacity, eSize, circular);
  queue_test_result_t testResult = QUEUE_TEST_PASS;
  //Run tests
  uint16_t subtestIdx = 1;
  uint16_t * subtestIdxPtr = &subtestIdx;
  Print(LOG_SUBTEST, "\r\n-------------------------------------------------\r\n");
  Print(LOG_SUBTEST, "Test %d: \r\n", *testNum);
  Queue_Test_Print(sQueue);

  if (Queue_Test_Result_Print(Queue_Test_Push(sQueue, eSize, capacity, capacity), subtestIdxPtr, QUEUE_SUBTEST) != QUEUE_TEST_PASS) //Push to capacity
  {
    testResult = QUEUE_TEST_FAIL;
  }
    
  Queue_Test_Print(sQueue);
   
  if (Queue_Test_Result_Print(Queue_Test_Discard(sQueue, capacity + 1), subtestIdxPtr, QUEUE_SUBTEST) != QUEUE_TEST_DISCARD_FAIL) //Discard past empty
  {
    testResult = QUEUE_TEST_FAIL;
  }
    
  Queue_Test_Print(sQueue);
 
  queueDeInitialize(sQueue);
  return testResult;
}

//Push past capacity, clear queue, pop one, push one, peek
static queue_test_result_t Queue_Test_Seq_10 (size_t eSize, size_t capacity, uint16_t * testNum, bool circular)
{
  /* Initialize small queue */
  seque_t * sQueue = queueInitialize(capacity, eSize, circular);
  queue_test_result_t testResult = QUEUE_TEST_PASS;
  //Run tests
  uint16_t subtestIdx = 1;
  uint16_t * subtestIdxPtr = &subtestIdx;
  Print(LOG_SUBTEST, "\r\n-------------------------------------------------\r\n");
  Print(LOG_SUBTEST, "Test %d: \r\n", *testNum);
  Queue_Test_Print(sQueue);

  if (circular)
  {
    if (Queue_Test_Result_Print(Queue_Test_Push(sQueue, eSize, capacity, capacity), subtestIdxPtr, QUEUE_SUBTEST) != QUEUE_TEST_PASS) //Push to capacity
    {
      testResult = QUEUE_TEST_FAIL;
    }
    
    Queue_Test_Print(sQueue);
    
    if (Queue_Test_Result_Print(Queue_Test_Push(sQueue, eSize, capacity, 1), subtestIdxPtr, QUEUE_SUBTEST) != QUEUE_TEST_PASS) //Push past capacity
    {
      testResult = QUEUE_TEST_FAIL;
    }
  }
  else  
  {
    if (Queue_Test_Result_Print(Queue_Test_Push(sQueue, eSize, capacity, capacity), subtestIdxPtr, QUEUE_SUBTEST) != QUEUE_TEST_PASS) //Push to capacity
    {
      testResult = QUEUE_TEST_FAIL;
    }
      
    Queue_Test_Print(sQueue); 
     
    if (Queue_Test_Result_Print(Queue_Test_Push(sQueue, eSize, capacity, 1), subtestIdxPtr, QUEUE_SUBTEST) != QUEUE_TEST_PUSH_FAIL) //Push past capacity
    {
      testResult = QUEUE_TEST_FAIL;
    }
  }
  Queue_Test_Print(sQueue);
  
  if (Queue_Test_Result_Print(Queue_Test_Clear(sQueue), subtestIdxPtr, QUEUE_SUBTEST) != QUEUE_TEST_PASS) //Clear queue
  {
    testResult = QUEUE_TEST_FAIL;
  }
  Queue_Test_Print(sQueue); 
  
  if (Queue_Test_Result_Print(Queue_Test_Pop(sQueue, eSize, 1), subtestIdxPtr, QUEUE_SUBTEST) != QUEUE_TEST_POP_FAIL) //Pop 1
  {
    testResult = QUEUE_TEST_FAIL;
  }
  Queue_Test_Print(sQueue);
  
  if (Queue_Test_Result_Print(Queue_Test_Push(sQueue, eSize, capacity, 1), subtestIdxPtr, QUEUE_SUBTEST) != QUEUE_TEST_PASS) //Push 1
  {
    testResult = QUEUE_TEST_FAIL;
  }
  
  Queue_Test_Print(sQueue);
 
  queueDeInitialize(sQueue);
  return testResult;
}

//Test that new queue is empty
static queue_test_result_t Queue_Test_Seq_11 (size_t eSize, size_t capacity, uint16_t * testNum, bool circular)
{
  /* Initialize small queue */
  seque_t * sQueue = queueInitialize(capacity, eSize, circular);
  queue_test_result_t testResult = QUEUE_TEST_PASS;
  //Run tests
  Print(LOG_SUBTEST, "\r\n-------------------------------------------------\r\n");
  Print(LOG_SUBTEST, "Test %d: \r\n", *testNum);
  Queue_Test_Print(sQueue);
  
  if (!queueIsEmpty(sQueue))
  {
    testResult = QUEUE_TEST_FAIL;
  }
  
  queueDeInitialize(sQueue);
  return testResult;
}

//Push to capacity, check that it is full
static queue_test_result_t Queue_Test_Seq_12 (size_t eSize, size_t capacity, uint16_t * testNum, bool circular)
{
  /* Initialize small queue */
  seque_t * sQueue = queueInitialize(capacity, eSize, circular);
  queue_test_result_t testResult = QUEUE_TEST_PASS;
  //Run tests
  uint16_t subtestIdx = 1;
  uint16_t * subtestIdxPtr = &subtestIdx;
  Print(LOG_SUBTEST, "\r\n-------------------------------------------------\r\n");
  Print(LOG_SUBTEST, "Test %d: \r\n", *testNum);
  Queue_Test_Print(sQueue);
  
  if (Queue_Test_Result_Print(Queue_Test_Push(sQueue, eSize, capacity, capacity), subtestIdxPtr, QUEUE_SUBTEST) != QUEUE_TEST_PASS) //Push to capacity
  {
    testResult = QUEUE_TEST_FAIL;
  }
   
  Queue_Test_Print(sQueue);
  
  if (queueIsEmpty(sQueue))
  {
    testResult = QUEUE_TEST_FAIL;
  }
  
  if (!queueIsFull(sQueue))
  {
    testResult = QUEUE_TEST_FAIL;
  }
  
  queueDeInitialize(sQueue);
  return testResult;
}

//Push 1, check size
static queue_test_result_t Queue_Test_Seq_13 (size_t eSize, size_t capacity, uint16_t * testNum, bool circular)
{
  /* Initialize small queue */
  seque_t * sQueue = queueInitialize(capacity, eSize, circular);
  queue_test_result_t testResult = QUEUE_TEST_PASS;
  //Run tests
  uint16_t subtestIdx = 1;
  uint16_t * subtestIdxPtr = &subtestIdx;
  Print(LOG_SUBTEST, "\r\n-------------------------------------------------\r\n");
  Print(LOG_SUBTEST, "Test %d: \r\n", *testNum);
  Queue_Test_Print(sQueue);
  
  if (Queue_Test_Result_Print(Queue_Test_Push(sQueue, eSize, capacity, 1), subtestIdxPtr, QUEUE_SUBTEST) != QUEUE_TEST_PASS) //Push to capacity
  {
    testResult = QUEUE_TEST_FAIL;
  }
  
  Queue_Test_Print(sQueue);
  
  if (queueGetSize(sQueue) != 1)
  {
    testResult = QUEUE_TEST_FAIL;
  }
  
  queueDeInitialize(sQueue);
  return testResult;
}

int main ( int argc, char *argv[] )
{
  size_t eSize, capacity;
  
  uint16_t testNum = 1;
  uint16_t * testNumPtr = &testNum;
  queue_test_result_t resultTable[2][NUM_TESTS];
  uint8_t ii;
  for (ii = 0; ii < argc; ii++)
  {
    if (argv[ii][0] == '-' && argv[ii][1] == 'v')
    {
      systemLogLevel = LOG_VERBOSE;
    }
    else if (argv[ii][0] == '-' && argv[ii][1] == 's')
    {
      systemLogLevel = LOG_SUBTEST;
    }
  }
  
  Print(LOG_NORMAL, "Queue Testing Module\r\n");
  
  if( argc == 3  || argc == 4) 
  {
    sscanf(argv[1], "%zu", &eSize);
    sscanf(argv[2], "%zu", &capacity);
    
    Print(LOG_NORMAL,"The argument supplied is %d, %d\n", eSize, capacity);
  }
  else if( argc > 4 ) 
  {
    Print(LOG_NORMAL,"Too many arguments supplied.\n");
    return 1;
  }
  else 
  {
    Print(LOG_NORMAL,"Using default values (10, 10)\n");
    eSize = 10;
    capacity = 10;
  }

  bool circular = false;
  
  for (int jj = 0; jj < 2; jj ++)
  {
    if (jj == 1)
    {
      circular = true;
    }
    
    for (int kk = 0; kk < NUM_TESTS; kk++)
    {
      resultTable[jj][kk] = testFunctions[kk](eSize, capacity, testNumPtr, circular);
      testNum++;
    }
    testNum = 1;
  }
  
  Print(LOG_NORMAL,"##################### \r\n");
  Print(LOG_NORMAL,"# Noncircular tests #\r\n");
  Print(LOG_NORMAL,"##################### \r\n");
  for (int jj = 0; jj < 2; jj ++)
  {
    if (jj == 1)
    {
      Print(LOG_NORMAL,"################## \r\n");
      Print(LOG_NORMAL,"# Circular tests #\r\n");
      Print(LOG_NORMAL,"################## \r\n");
    }
    
    for (int kk = 0; kk < NUM_TESTS; kk ++)
    {
      Queue_Test_Result_Print(resultTable[jj][kk], testNumPtr, QUEUE_TEST);
    }
    testNum = 1;
  }
  return 0;
}