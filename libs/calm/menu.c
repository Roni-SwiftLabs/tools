/**
 * @file menu.c
 * @author Daniel Bujak
 * @date August 23, 2018
 * @brief Command line interface menu contents
 *
 */

/* Includes ------------------------------------------------------------------*/

#include "cli.h"
#include "menu.h"
#include "calm.h"


#include <stdlib.h>

#ifndef UNUSED
#define UNUSED(x) (void)(x)
#endif

/* CLI Library Variables */
cli_menu_t * menuEntries[CLI_MENU_COUNT];

/* CLI Functions */
static void No_Action ( uint8_t * d , uint8_t len);

/* System control functions */

/* Status Display Functions */
static void Menu_Display_Help        ( void );



/* --------- CLI Library Menu Entries --------- */

/* Main Menu Entries */
cli_menu_entry_t entriesMain[] = {
  {.string = ".",                   .menuDest = CLI_MENU_MAIN,     .cbFunc = No_Action},
  {.string = "Submenu",             .menuDest = CLI_MENU_SUBMENU,  .cbFunc = No_Action},
  {.string = "Input",               .menuDest = CLI_MENU_INPUT,    .cbFunc = No_Action},
  {.string = "Help",                .menuDest = CLI_MENU_HELP,     .cbFunc = No_Action} };

/* Input Menu Entries */
cli_menu_entry_t entriesSubmenu[] = {
  {.string = ".",                   .menuDest = CLI_MENU_MAIN,     .cbFunc = No_Action},
  {.string = "Print",               .menuDest = CLI_MENU_PRINT,    .cbFunc = No_Action} };

/* --------- CLI Library Menu Structures --------- */

/* Main Menu Type */
cli_menu_t menuMain = {
    .header     = "Main (space=pause, ctrl-x=clear)",
    .menuType   = CLI_MENU_TYPE_ENTRIES,
    .numEntries = sizeof(entriesMain)/sizeof(cli_menu_entry_t),
    .pEntries   = entriesMain,
    .setupFunc  = NULL };

/* Help Menu */
cli_menu_t menuHelp = {
    .header     = "",
    .menuType   = CLI_MENU_TYPE_INFO,
    .numEntries = 0,
    .pEntries   = NULL,
    .setupFunc  = Menu_Display_Help };

/* Input Menu Type */
cli_menu_t menuInput = {
    .header     = "Input Value:",
    .menuType   = CLI_MENU_TYPE_INPUT,
    .numEntries = 0,
    .pEntries   = NULL,
    .setupFunc  = NULL };
/* Sub Menu Type */
cli_menu_t menuSubmenu = {
    .header     = "SubMenu",
    .menuType   = CLI_MENU_TYPE_ENTRIES,
    .numEntries = sizeof(entriesSubmenu)/sizeof(cli_menu_entry_t),
    .pEntries   = entriesSubmenu,
    .setupFunc  = NULL };

/* Print input menu Type */
cli_menu_t menuPrint = {
    .header     = "",
    .menuType   = CLI_MENU_TYPE_INFO,
    .numEntries = 0,
    .pEntries   = NULL,
    .setupFunc  = Menu_Display_Print };

/**
  * @brief  Initialize the CLI system
  * @param  None
  * @retval None
  */
cli_menu_t ** Menu_Initialize ( void )
{
  menuEntries[CLI_MENU_MAIN]     = &menuMain;
  menuEntries[CLI_MENU_SUBMENU]  = &menuSubmenu;
  menuEntries[CLI_MENU_HELP]     = &menuHelp;
  menuEntries[CLI_MENU_INPUT]    = &menuInput;
  menuEntries[CLI_MENU_PRINT]    = &menuPrint;

  return menuEntries;
}


/**
  * @brief  UnInitialize the CLI system
  * @param  None
  * @retval None
  */
void CLI_Uninit ( void )
{

}


/************************************************************************
 *************************** CLI Menu Functions *************************
 ************************************************************************/

static void No_Action ( uint8_t * d , uint8_t len)
{

}



/************************************************************************
 ************************ Status Display Functions **********************
 ************************************************************************/

/**
  * @brief  Display system status menu
  * @param  None
  * @retval None
  */
void Menu_Display_Status ( void )
{
  LOGC("Example Status Menu\r\n")
  LOGC("\r\n");
  LOGC("Press any navigation key to return.\r\n");
}

void Menu_Display_Main ( void )
{
  LOGC("Example Status Menu\r\n")
  LOGC("\r\n");
  LOGC("Press any navigation key to return.\r\n");
}

void Menu_Display_Print ( void )
{
  LOGC("Example Print menu\r\n")
  LOGC("\r\n");
  LOGC("Press any navigation key to return.\r\n");
}

/**
  * @brief  Display system help s
  * @param  None
  * @retval None
  */
static void Menu_Display_Help (void)
{
  LOGC("\r*** Example Help Menu ***\r\n");
  LOGC("  Space        : Toggle Log Pause\r\n");
  LOGC("  CTRL-C       : Clear Log\r\n");
  LOGC("  Arrow Keys   : Navigate Menu\r\n");
  LOGC("  Enter Key    : Accept input or selection\r\n");
  LOGC("  + / - Key    : Adjust log level\r\n");
  LOGC("\r\n");
  LOGC("Press any navigation key to return.\r\n");
}

