#include "cli.h"
#include "calm.h"
#include "menu.h"
#include "command.h"
#include "calm_test.h"

#include <termios.h>
#include <unistd.h>
#include <stdio.h>
#include <stdlib.h>
#include <time.h>
#include <pthread.h>

/* Sample command inputs */
uint8_t input1[5] = {0x04,0x10,0x01,0x02,0x03};
uint8_t input2[4] = {0x03,0x20,0x31,0x32};
uint8_t input3[6] = {0x05,0x30,0x61,0x22,0x33, 0x94};


/* pthread functions */
void *readCLIInputThread  (void *vargp) ;
void *readCMDInputThread  (void *vargp) ;
void *tickThread          (void *vargp) ;
void *logThread           (void *vargp) ;

/* Test log printing function */
void  Test_Log_Prints     ( void )      ;

/* Tick used to represent time */
uint32_t tick = 0;

char getch(void);

/* Used as timeFunc */
uint32_t GetTick(void)
{
  return tick;
}
/* Custom key callback function */
void register_Func(void)
{
  LOGI( "Custom key function \r\n");
}

keymapCB_t register_Func_CB = &register_Func;

/**
  * @brief  State switch for initializing CaLM
  * @param  mode - CaLM mode ( CLI, Command, Log )
  * @retval None
  */
void Main_Init(calm_mode_t mode)
{
  calm_init_t calmInit;
  calmInit.mode = mode;
  calmInit.scrollback = 20;
  calmInit.uartBuffSize = 100;
  calmInit.logLevel = LOG_DEBUG;

  switch( mode )
  {
    case CALM_MODE_CLI:
    calmInit.cmdFunc = NULL;
    calmInit.timeFunc = &GetTick;
    calmInit.period = 1000;
    calmInit.lineSize = 350;
    calmInit.menu = Menu_Initialize();
    break;

    case CALM_MODE_CMD:
    calmInit.cmdFunc = &Command_Handler;
    calmInit.logLevel = LOG_CMD;
    calmInit.timeFunc = NULL;
    calmInit.period = 0;
    calmInit.lineSize = 250;
    calmInit.menu = NULL;
    break;

    case CALM_MODE_LOG:
    calmInit.cmdFunc = NULL;
    calmInit.timeFunc = &GetTick;
    calmInit.period = 1000;
    calmInit.lineSize = 150;
    calmInit.menu = NULL;
    break;
  }
  Calm_Initialize(calmInit);
  /* Register a custom key */
  Calm_Register_Key(REGISTER_KEY, register_Func_CB);
}

/**
  * @brief  pthread function which reads keystrokes and pushes them to CaLM for CLI mode
  * @param  None
  * @retval None
  */
void *readCLIInputThread(void *vargp) 
{ 
  char ch;
  while(1)
  {
    ch = getch();
    Calm_Usart_Rx(ch);
  }
  return NULL; 
} 

/**
  * @brief  pthread function which reads keystrokes and pushes corresponding 
  * commands to CaLM in command mode 
  * @param  None
  * @retval None
  */
void *readCMDInputThread(void *vargp) 
{ 
  char ch;

  while(1)
  {
    ch = getch();
    if (ch == '1')
    {
      for(int ii = 0; ii < 5; ii++)
      {
        Calm_Usart_Rx(input1[ii]);
      }
    }
    else if (ch == '2')
    {
      for(int jj = 0; jj < 4; jj++)
      {
        Calm_Usart_Rx(input2[jj]);
      }
    }
    else if (ch == '3')
    {
      for(int kk = 0; kk < 6; kk++)
      {
        Calm_Usart_Rx(input3[kk]);
      }
    }
  }
return NULL; 
} 

/**
  * @brief  pthread function to simulate clock ticks
  * @param  None
  * @retval None
  */
void *tickThread(void *vargp) 
{ 
  while(1)
  {
    tick += 1;
    usleep(1);
  }
  return NULL; 
} 

/**
  * @brief  pthread function to periodically print logs
  * @param  None
  * @retval None
  */
void *logThread(void *vargp)
{
  while(1)
  {
    Test_Log_Prints();
    sleep(2);
  }
}

/**
  * @brief  Prints logs at all log levels
  * @param  None
  * @retval None
  */
void Test_Log_Prints ( void )
{
  LOGE("Error Log \r\n");
  LOGW("Warning Log \r\n");
  LOGI("Information Log \r\n");
  LOGD("Debug Log \r\n");
  LOGV("Verbose Log \r\n");
  LOGVV("VVerbose Log \r\n");
}



int main(int argc, char *argv[])
{
  calm_mode_t mode = CALM_MODE_CLI;
  /* Parse arguements for command or log mode */
  if (argc == 2)
  {
    if (argv[1][0] == '-')
    { 
      if (argv[1][1] == 'c')  
      {
        mode = CALM_MODE_CMD;
      }
      else if (argv[1][1] == 'l')
      {
        mode = CALM_MODE_LOG;
      }
    } 
  }
  /* Initialize CaLM */
  Main_Init(mode);
  /* Initialize pthreads */
  if ( mode == CALM_MODE_CLI )
  {
    pthread_t readCLIInput_id, tick_id; 
    pthread_create(&readCLIInput_id, NULL, readCLIInputThread, NULL); 
    pthread_create(&tick_id, NULL, tickThread, NULL); 
  }
  else if ( mode == CALM_MODE_CMD )
  {
    pthread_t readCMDInput_id;
    pthread_create(&readCMDInput_id, NULL, readCMDInputThread, NULL); 
  }
  else if ( mode == CALM_MODE_LOG )
  {
    pthread_t readCLIInput_id, tick_id, log_id; 
    pthread_create(&readCLIInput_id, NULL, readCLIInputThread, NULL); 
    pthread_create(&tick_id, NULL, tickThread, NULL); 
    pthread_create(&log_id, NULL, logThread, NULL);
  }

  /* Main loop */
  while(1)
  {
    Calm_Main();
  }

}

/**
  * @brief  Reads keystrokes without waiting for enter key
  * @param  None
  * @retval ch - character read
  */
char getch(void)
{
  struct termios oldattr, newattr;
  char ch;
  tcgetattr( STDIN_FILENO, &oldattr );
  newattr = oldattr;
  newattr.c_lflag &= ~( ICANON | ECHO );
  tcsetattr( STDIN_FILENO, TCSANOW, &newattr );
  ch = getchar();
  tcsetattr( STDIN_FILENO, TCSANOW, &oldattr );
  return ch;
}