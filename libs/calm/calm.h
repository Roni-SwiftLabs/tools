/**
 * @file calm.h
 * @author Daniel Bujak
 * @date August 23, 2018
 * @brief Command line interface, And Logging Module header
 *
 */

/* Define to prevent recursive inclusion -------------------------------------*/
#ifndef __CALM_H
#define __CALM_H

#ifdef __cplusplus
 extern "C" {
#endif

#include "cli.h"

#include <string.h>
#include <stdint.h>
#include <stdbool.h>

#define CALM_ENABLE             /**< Comment out to disable logging module entirely */
#define XTERM_SUPPORT           /**< Enable for colours */

#define __FILENAME__ (strrchr(__FILE__, '\\') ? strrchr(__FILE__, '\\') + 1 : __FILE__)

/* Hex value count */
#define HEX_VALUE_CNT           17

/* Log levels */
#define LOG_ALWAYS              0
#define LOG_ERROR               1
#define LOG_WARN                2
#define LOG_INFO                3
#define LOG_DEBUG               4
#define LOG_VERBOSE             5
#define LOG_VVERBOSE            6
#define LOG_CLI                 7
#define LOG_CMD                 8

/* Set logging parameters */
#define LOG_STD_LEVEL           LOG_DEBUG
#define SCROLLBACK              20    /* Default Scrollback value */
#define LOG_LINE_SIZE           150   /* Default Line size value */
#define KEY_MAP_MAX             3
#define UART_BUFF_SIZE          10    /* Default UART Buffer size value */

/* xTerm Commands */
#define XTERM_CLEAR_SCREEN      "\033[2J"
#define XTERM_CURSOR_HOME       "\033[H"

/* Logging colours */
#define RTT_CTRL_BG_BLACK       "\033[24;40m"
#define RTT_CTRL_BG_RED         "\033[24;41m"
#define RTT_CTRL_BG_GREEN       "\033[24;42m"
#define RTT_CTRL_BG_YELLOW      "\033[24;43m"
#define RTT_CTRL_BG_BLUE        "\033[24;44m"
#define RTT_CTRL_BG_MAGENTA     "\033[24;45m"
#define RTT_CTRL_BG_CYAN        "\033[24;46m"
#define RTT_CTRL_BG_WHITE       "\033[24;47m"

#define RTT_CTRL_TEXT_BLACK     "\033[1;30m"
#define RTT_CTRL_TEXT_RED       "\033[1;31m"
#define RTT_CTRL_TEXT_GREEN     "\033[1;32m"
#define RTT_CTRL_TEXT_YELLOW    "\033[1;33m"
#define RTT_CTRL_TEXT_BLUE      "\033[1;34m"
#define RTT_CTRL_TEXT_MAGENTA   "\033[1;35m"
#define RTT_CTRL_TEXT_CYAN      "\033[1;36m"
#define RTT_CTRL_TEXT_WHITE     "\033[1;37m"

/* Disable all macros by default */
#undef ENABLE_LOG_MACROS
#undef ENABLE_CMD_MACROS

/* Determine which macros should be enabled */
#ifdef CALM_ENABLE
  #ifdef COMMAND_SUPPORT
    #define ENABLE_CMD_MACROS
    #define LOG_LEVEL               LOG_CMD
    #define LINE_SIZE               250
    #undef  ENABLE_LOG_MACROS
  #else
    #define ENABLE_LOG_MACROS
    #define LOG_LEVEL               LOG_STD_LEVEL
    #define LINE_SIZE               LOG_LINE_SIZE
  #endif /* COMMAND_SUPPORT */
#endif /* CALM_ENABLE */


/* Standard Logging Macros */
#ifdef ENABLE_LOG_MACROS
  #ifdef XTERM_SUPPORT
    #define LOGC(_prefix_, ...)    Calm_Print(RTT_CTRL_BG_BLACK RTT_CTRL_TEXT_WHITE   _prefix_, ##__VA_ARGS__);                                   /* CLI */
    #define LOGE(_prefix_, ...)    Calm_Log(__FILENAME__, LOG_ERROR,    true, RTT_CTRL_BG_RED   RTT_CTRL_TEXT_YELLOW  _prefix_, ##__VA_ARGS__);   /* Error */
    #define LOGW(_prefix_, ...)    Calm_Log(__FILENAME__, LOG_WARN,     true, RTT_CTRL_BG_BLACK RTT_CTRL_TEXT_YELLOW  _prefix_, ##__VA_ARGS__);   /* Warning */
    #define LOGI(_prefix_, ...)    Calm_Log(__FILENAME__, LOG_INFO,     true, RTT_CTRL_BG_BLACK RTT_CTRL_TEXT_CYAN    _prefix_, ##__VA_ARGS__);   /* Information */
    #define LOGD(_prefix_, ...)    Calm_Log(__FILENAME__, LOG_DEBUG,    true, RTT_CTRL_BG_BLACK RTT_CTRL_TEXT_BLUE    _prefix_, ##__VA_ARGS__);   /* Debug */
    #define LOGV(_prefix_, ...)    Calm_Log(__FILENAME__, LOG_VERBOSE,  true, RTT_CTRL_BG_BLACK RTT_CTRL_TEXT_MAGENTA _prefix_, ##__VA_ARGS__);   /* Verbose */
    #define LOGVV(_prefix_, ...)   Calm_Log(__FILENAME__, LOG_VVERBOSE, true, RTT_CTRL_BG_BLACK RTT_CTRL_TEXT_MAGENTA _prefix_, ##__VA_ARGS__);   /* Verbose */
    #define LOGP(_prefix_, ...)    Calm_Print(RTT_CTRL_BG_BLACK RTT_CTRL_TEXT_WHITE   _prefix_, ##__VA_ARGS__);                                   /* Print (Print direct to screen) */
  #else
    #define LOGC(_prefix_, ...)    Calm_Print(_prefix_, ##__VA_ARGS__);                               /* CLI */
    #define LOGE(_prefix_, ...)    Calm_Log(__FILE__, LOG_ERROR,    true, _prefix_, ##__VA_ARGS__);   /* Error */
    #define LOGW(_prefix_, ...)    Calm_Log(__FILE__, LOG_WARN,     true, _prefix_, ##__VA_ARGS__);   /* Warning */
    #define LOGI(_prefix_, ...)    Calm_Log(__FILE__, LOG_INFO,     true, _prefix_, ##__VA_ARGS__);   /* Information */
    #define LOGD(_prefix_, ...)    Calm_Log(__FILE__, LOG_DEBUG,    true, _prefix_, ##__VA_ARGS__);   /* Debug */
    #define LOGV(_prefix_, ...)    Calm_Log(__FILE__, LOG_VERBOSE,  true, _prefix_, ##__VA_ARGS__);   /* Verbose */
    #define LOGVV(_prefix_, ...)   Calm_Log(__FILE__, LOG_VVERBOSE, true, _prefix_, ##__VA_ARGS__);   /* Verbose */
    #define LOGP(_prefix_, ...)    Calm_Print(_prefix_, ##__VA_ARGS__);                               /* Print (Print direct to screen) */
  #endif /* XTERM_SUPPORT */
#else
  #define LOGC(_prefix_, ...)
  #define LOGE(_prefix_, ...)
  #define LOGW(_prefix_, ...)
  #define LOGI(_prefix_, ...)
  #define LOGD(_prefix_, ...)
  #define LOGV(_prefix_, ...)
  #define LOGVV(_prefix_, ...)
  #define LOGP(_prefix_, ...)
#endif /* ENABLE_LOG_MACROS */

#ifdef ENABLE_CMD_MACROS
  #define LOGCMD(_prefix_, ...)  Calm_Log("", LOG_CMD, false, _prefix_, ##__VA_ARGS__);
  #undef  LOGP
  #define LOGP(_prefix_, ...)    Calm_Print(_prefix_, ##__VA_ARGS__);
#else
  #define LOGCMD(_prefix_, ...)
#endif /* ENABLE_CMD_MACROS */

/**
 * @brief CaLM input mode type
 */
typedef enum
{
  CALM_INMODE_NAV,                  /**< Standard menu navigation commands */
  CALM_INMODE_CLI_INPUT            /**< Input data for CLI */
} calm_inmode_t;


/* Custom keymap callback function typedef */
typedef void (* keymapCB_t)( void );

/* Command mode callback function typedef */
typedef void (* commandCB_t)( uint8_t * data, uint8_t len );


/**
 * @brief CaLM Key Mapping type
 */
typedef struct
{
  uint8_t        key;                 /**< Keystroke to map */
  keymapCB_t     cbFunc;              /**< Function to call back to */
} calm_keymap_t;


/**
 * @brief CaLM mode
 */
typedef enum 
{
  CALM_MODE_CMD,
  CALM_MODE_LOG,
  CALM_MODE_CLI
} calm_mode_t;


/**
 * @brief CaLM Initialization type
 */
typedef struct
{
  uint8_t        logLevel;
  uint8_t        scrollback;
  calm_mode_t    mode;
  uint16_t       lineSize;
  uint16_t       uartBuffSize;
  uint16_t       period;              /**< UI refresh period */
  uint32_t    (* timeFunc)();         /**< Pointer to function that returns timestamp */
  commandCB_t    cmdFunc;             /**< Pointer to function to handle command inputs */
  cli_menu_t  ** menu;                /**< Pointer to array of menus */ 
} calm_init_t;


/* Exported Functions */
void Calm_Main                        ( void );
void Calm_Redraw                      ( void );
void Calm_Print                       ( char *format, ... );
void Calm_Log                         ( const char * file, uint8_t log_level, bool header, char *format, ... );
void Calm_Print_Buffer                ( uint8_t logLevel, uint8_t * buff, uint16_t len, char * hdr );
void Calm_Usart_Rx                    ( uint8_t received_char );
void Calm_DeInitialize                ( void );
int  Calm_Initialize                  ( calm_init_t init );
bool Calm_Register_Key                ( uint8_t key, keymapCB_t cbFunc );

#ifdef __cplusplus
}
#endif
#endif /* __CALM_H */
