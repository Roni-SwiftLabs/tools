/**
 * @file menu.h
 * @author Daniel Bujak
 * @date August 23, 2018
 * @brief Command line interface menu definitions
 *
 */

/* Define to prevent recursive inclusion -------------------------------------*/
#ifndef __MENU_H
#define __MENU_H

#ifdef __cplusplus
 extern "C" {
#endif

#include <stdint.h>
#include <stdbool.h>
#include "cli.h"

/**
 * @brief Command line interface menus
 */
typedef enum
{
  CLI_MENU_MAIN = 0,                    /**< Main menu */
  CLI_MENU_SUBMENU,
  CLI_MENU_HELP,                        /**< System help menu */
  CLI_MENU_INPUT,                       /**< User input screen */
  CLI_MENU_PRINT,                       
  CLI_MENU_COUNT
} menu_list_t;


/* Function Prototypes */
void            Menu_Display_Status ( void );
void            Menu_Display_Print ( void );
cli_menu_t **   Menu_Initialize    ( void );
void            Menu_Uninit         ( void );


#ifdef __cplusplus
}
#endif
#endif /* __MENU_H */
