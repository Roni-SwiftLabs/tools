/**
 * @file calm.c
 * @author Daniel Bujak
 * @date August 23, 2018
 * @brief Command line interface, And Logging Module
 *
 */

#include "calm.h"
#include "queue.h"

#include <stdarg.h>
#include <string.h>
#include <stdio.h>
#include <stdlib.h>


seque_t *      hLogQueue;    /* Log queue handle */
seque_t *      hRxQueue;     /* USART queue hande */

/* Pointer to timestamp function */
static uint32_t (* timeFunc)();

/* Local variables */
static bool          calmInitialized   = false;
static bool          logPause          = false;           /* Display paused flag */
static bool          dispRefresh       = false;           /* Display refresh flag */
static uint8_t       sysLogLevel       = LOG_DEBUG; 
static uint8_t       scrollback        = SCROLLBACK;      /* Default Scrollback value */
static uint16_t      lineSize          = LINE_SIZE;       /* Default Linesize value */
static uint16_t      uartBuffSize      = UART_BUFF_SIZE;  /* Default Uart buffer size value */
static uint32_t      timestamp         = 0;
static uint16_t      refreshPer        = 1000;
static calm_inmode_t inputMode         = CALM_INMODE_NAV;

/* User input variables */
static uint8_t    cliRxCnt = 0;
static uint8_t    cliRxLen = 0;
static uint8_t    cliInputLen = 0;
static uint8_t    cliRxBuff[ CLI_MSG_MAX_LEN ];       /* Buffer to store raw character input array from user */
static uint8_t    cliInputBuff[CLI_MAX_INPUT_LEN];    /* Buffer to store parsed array input by user */

/* Custom Key Mapping */
static uint8_t       keyMapCount = 0;
static calm_keymap_t keyMap[KEY_MAP_MAX];

/* Commanding mode callback function */
commandCB_t cmdCbFunc;

/* Module operation mode */
calm_mode_t mode;

/* Index strings for logging readability */
const char logLevelStrs   [8][10]  = { {"DIRECT"}, {"ERROR"}, {"WARN"}, {"INFO"}, {"DEBUG"}, {"VERBOSE"}, {"VVERBOSE"}, {"CLI"} };

/* Private functions */
static void    Input_Parse             ( uint8_t * cliRxBuff, uint8_t len );
static void    Handle_Input            ( uint8_t ch );
static void    Handle_Command_Input    ( uint8_t ch );
static void    Handle_CLI_Input        ( uint8_t ch );
static void    Handle_Log_Input        ( uint8_t ch );
static void    Prompt_Input            ( void );
static void    Input_Clear             ( void );
static void    Log_Redraw              ( void );
static int     Print_Timestamp         ( char * buff );
static int     Print_Module_Name       ( const char * file, char * buff );
static void    Clear_Screen            ( void );
static void    Calm_Refresh_On_Next    ( void );

/**
  * @brief  Module main function
  * @param  None
  * @retval None
  */
void Calm_Main ( void )
{
  if ( calmInitialized )
  {
    uint8_t ch;
    uint32_t newTime;

    /* Get any pending characters */
    while ( !queueIsEmpty(hRxQueue) )
    {
      if ( queuePop(hRxQueue, &ch) == QUEUE_OK )
      {
        Handle_Input(ch);
      }
    }

    /* Main state switch */
    switch (mode)
    {
      /* Command Mode */
      case CALM_MODE_CMD:
        break;

      /* CLI Mode */
      case CALM_MODE_CLI:
        newTime = timeFunc();
        /* Check if refresh rate has been reached for info type menu */
        if ( ( (newTime - timestamp) >  refreshPer ) && CLI_Menu_Type_Get() == CLI_MENU_TYPE_INFO )
        {
          timestamp = newTime;
          Calm_Refresh_On_Next();
        }
        break;

      /* Log Mode */
      case CALM_MODE_LOG:
        break;

      default:
        break;
    }

    /* Check if refresh flag was set */
    if ( dispRefresh ) 
    {
      dispRefresh = false;
      Calm_Redraw();
    }
  }
}


static void Calm_Refresh_On_Next ( void )
{
  switch (mode)
  {
    /* CLI Mode */
    case CALM_MODE_CLI:
      if ( !logPause )
      {
        dispRefresh = true;
      }
      break;

    /* Log Mode */
    case CALM_MODE_LOG:
      break;

    default:
      dispRefresh = true;
      break;
  }
}


/************************************************************************
 *************************** printf Functions ***************************
 ************************************************************************/

/**
  * @brief  Print function for direct print
  * @param  * format  String to print
  * @param  ...       Format arguements
  * @retval None
  */
void Calm_Print ( char *format, ... )
{
  va_list args;
  va_start(args, format);

  if ( calmInitialized )
  {
    vprintf(format, args);
  }
  va_end(args);
}


/**
  * @brief  Function to print to log buffer
  * @param  log_level Priority level of message to be printed
  * @param  * format  String to print
  * @param  ...       Format arguements
  * @retval None
  */
void Calm_Log ( const char* file, uint8_t log_level, bool header, char *format, ... )
{
  va_list args;
  va_start(args, format);
  char * sBuff = malloc(lineSize * sizeof(char));
  char * pBuff = sBuff;
  
  /* Check if we should log this */
  if ( ( calmInitialized ) && ( log_level <= sysLogLevel ) )
  {
    int tsLen = 0;
    int modNameLen = 0;

    if ( header )
    {
      /* Print timestamp and capture length */
      tsLen = Print_Timestamp(pBuff);
      if ( tsLen < 0) 
      {
        tsLen = 0;
      }
      pBuff += tsLen;
    
      /* Print module name and capture length */
      modNameLen = Print_Module_Name( file , pBuff );
      if ( modNameLen < 0) 
      {
        modNameLen = 0;
      }
      pBuff += modNameLen;
    }

    /* Determine total length of print string */
    uint16_t len = tsLen + modNameLen + vsnprintf(pBuff, (lineSize - tsLen - modNameLen - 1), format, args);

    /* With CLI write to buffer. Otherwise print direct */
    if ( mode == CALM_MODE_CLI )
    {
      queuePush(hLogQueue, sBuff, len + 1); /* Add 1 to len to include termination char */
      Calm_Refresh_On_Next();
    }
    else if ( mode == CALM_MODE_CMD )
    {
      Calm_Print("%s", sBuff);
    }
    else
    {
      if ( !logPause )
      {
        LOGP("%s", sBuff);
      }
    }
  }
  va_end(args);
  free(sBuff);
}


/**
  * @brief  Function to print a buffer to the system logs
  * @param  None
  * @retval None
  */
void Calm_Print_Buffer ( uint8_t logLevel, uint8_t * buff, uint16_t len, char * hdr )
{
  /* Check if we should log this */
  if ( ( calmInitialized ) && ( logLevel <= sysLogLevel ) )
  {
    /* In CLI mode add to log queue without adding timestamp or module name */
    if ( mode == CALM_MODE_CLI )
    {
      char * pStart = malloc(lineSize * sizeof(char));
      int ii;
      char * pBuff = pStart;
      char * pEnd  = pStart + lineSize * sizeof(char);
      
      /* Write header to string */
      pBuff += sprintf(pBuff, "%s", hdr);
      
      /* No length specified indicates empty buffer */
      if (len == 0)
      {
        pBuff += sprintf(pBuff, "N/A\r\n");
      }
      else
      {
        for (ii= 0; ii < len; ii++)
        {
          /* Leave 20 for the last write + termination characters (; END\r\n\0) */
          if (pBuff + 20 < pEnd)
          {
            pBuff += sprintf(pBuff, "0x%x,", buff[ii]);
          }
        }
        /* If we couldn't print the whole buffer indicate that */
        if ( pBuff + 20 >= pEnd )
        {
          pBuff += sprintf(pBuff - 1, ", ...;\r\n");
        }
        else
        {
          pBuff += sprintf(pBuff - 1, " END;\r\n");
        }
      }
      queuePush(hLogQueue, pStart, (uint8_t)(pBuff - pStart) ); /* Length is current pointer - start of buffer */
      Calm_Refresh_On_Next();

      free(pStart);
    }

    /* In CMD or LOG mode print direct */
    else if ( ( mode == CALM_MODE_CMD && logLevel == LOG_CMD ) || ( mode == CALM_MODE_LOG ))
    {
      uint16_t ii;
      
      Calm_Print("%s", hdr);
      for ( ii = 0; ii < len; ii++ )
      {
        Calm_Print("0x%.2x, ", buff[ii]);
      }
      Calm_Print("END;\r\n");
    }
  }
}


/************************************************************************
 *************************** Helper Functions ***************************
 ************************************************************************/

/**
  * @brief  Prints current timestamp to buffer
  * @param  Buffer to print to
  * @retval Number of bytes printed
  */
static int Print_Timestamp ( char * buff )
{
  uint32_t ts = timeFunc();

  uint16_t  m = ( ts / ( 60 * 1000 ) );           /* minutes */
  uint8_t   s = ( ts / (      1000 ) ) % 60;      /* seconds */
  uint16_t ms = ( ts % 1000 );                    /* milliseconds */

  return sprintf(buff, "[%d:%.2d.%.4d] ", m, s, ms);
}

static int Print_Module_Name ( const char * file, char * buff )
{
  return sprintf(buff, "[%s] ", file);
}

/**
  * @brief  Redraws the UI 
  * @retval None
  */
void Calm_Redraw ( void )
{
  switch ( mode )
  {
    case CALM_MODE_CLI:

      Clear_Screen();
      if ( inputMode == CALM_INMODE_CLI_INPUT )
      {
        Prompt_Input();
      }
      else
      {
        CLI_Redraw();
      }
      Log_Redraw();
      break;

    case CALM_MODE_LOG:
      Clear_Screen();
      break;

    default:
      break;
  }  
}


/**
  * @brief  Function to adjust logging level on the fly
  * @param  None
  * @retval None
  */
static void Calm_Adjust_Level ( int8_t lvl )
{
  sysLogLevel += lvl;

  /* Don't increase past limits */
  if ( sysLogLevel < LOG_ERROR )
  {
    sysLogLevel = LOG_ERROR;
  }
  if ( sysLogLevel > LOG_VVERBOSE )
  {
    sysLogLevel = LOG_VVERBOSE;
  }

  LOGE( "Log level set to: %s\r\n", logLevelStrs[sysLogLevel] );
}


/**
  * @brief  Function to clear screen and home cursor
  * @param  None
  * @retval None
  */
static void Clear_Screen ( void )
{
  Calm_Print("%s", XTERM_CLEAR_SCREEN);
  Calm_Print("%s", XTERM_CURSOR_HOME);
}


/**
  * @brief  Toggles the periodic auto refresh feature.
  * @param  None
  * @retval None
  */
static void Toggle_Refresh_Enable ( void )
{
  logPause = !logPause;
  
  if (mode == CALM_MODE_CLI)
  {
  /* Force immediate update if refresh was enabled */
    if ( !logPause )
    {
      LOGW("CLI UnPaused\r\n");
      Calm_Refresh_On_Next();
    }
    else
    {
      LOGW("CLI Paused\r\n");
      Calm_Redraw();
    }
  }
  else if ( mode == CALM_MODE_LOG )
  {
    if ( logPause )
    {
      LOGP("Log paused \r\n");
    }
    else if ( !logPause)
    {
      LOGP("Log resumed \r\n");
    }
  }
}


/**
  * @brief  Function to clear screen and home cursor
  * @param  None
  * @retval None
  */
static void Calm_CLI_Log_Clear ( void )
{
  if ( mode == CALM_MODE_CLI )
  {
    queueClear(hLogQueue);
    Calm_Refresh_On_Next();
  }
  else
  {
    Clear_Screen();
  }
}


/**
  * @brief  Function to re-print all messages in log queue
  * @param  None
  * @retval None
  */
static void Log_Redraw ( void )
{
  char * tail, * nextData;

  /* Start peeking from the head */
  queueHead(hLogQueue, (void **)&nextData);

  LOGP("--------------------------------------\r\n");
  LOGP("System Log: %s\r\n\r\n", logLevelStrs[sysLogLevel]);

  /* Check to make sure queue is not empty */
  if ( queueTail( hLogQueue, (void **)&tail ) == QUEUE_OK )
  {
    /* Keep peeking data until we loop */
    while ( nextData != tail ) 
    {
      LOGP( "%s", nextData );
      nextData = queueNextData( hLogQueue, nextData );
    }
    /* Print the tail as well */
    LOGP( "%s", nextData );
  }
}


/************************************************************************
 *********************** Input Parsing Functions ************************
 ************************************************************************/


/**
  * @brief   Input request callback from the CLI
  * @details CLI will call this function when it needs user input. CaLM will call 
  *          CLI_Read_Input() with the data when it's ready/
  * @retval  None
  */
void Calm_Cli_Input_Req ( void )
{
  inputMode = CALM_INMODE_CLI_INPUT;
  Input_Clear();
}


/**
  * @brief  Store character received on CLI
  * @param  c Character received
  * @retval None
  */
static void Input_Add_Char ( uint8_t ch )
{  
  cliRxBuff[cliRxCnt] = ch;
  cliRxCnt++;
}


/**
  * @brief  Clear CLI input buffer
  * @param  None
  * @retval None
  */
static void Input_Clear ( void )
{
  cliRxCnt = 0;
  cliInputLen = 0;
  memset(cliRxBuff, 0, sizeof(cliRxBuff));
}


/**
  * @brief  Delete 1 character from CLI input buffer
  * @param  None
  * @retval None
  */
static void Input_Delete_Char ( void )
{
  if ( cliRxCnt > 0 )
  {
    cliRxBuff[cliRxCnt - 1] = 0;
    cliRxCnt--;
  }
}


/**
  * @brief  Function called from USART IRQ Handler when data is received on
  * CLI interface.
  * @param  None
  * @retval None
  */
void Calm_Usart_Rx ( uint8_t received_char )
{
  if ( calmInitialized == true )
  {
    /* If CLI support is enabled, parse the input */
    if ( queuePush(hRxQueue, &received_char, 1) != QUEUE_OK )
    {
      LOGW ("CALM Rx push to queue failed\r\n");
    }
  }
}

/**
  * @brief  Handles input to CLI based on input mode
  * @param  Character being handled
  * @retval None
  */
static void Handle_CLI_Input ( uint8_t ch )
{
  static uint8_t esc_cnt = 0;
  /* Input menu */
  if ( inputMode == CALM_INMODE_CLI_INPUT )
  {
    /* Return Key - Parse Buffer in CLI input mode */
    if ( ch == ENTER_KEY )
    {
      /* Make sure data was entered */
      if ( cliRxCnt > 0 )
      {
        /* Add terminating comma and parse */
        cliRxBuff[cliRxCnt++] = ',';
        Input_Parse(cliRxBuff, cliRxCnt);
        CLI_Input_Ready ( cliInputBuff, cliInputLen );
      }
      else
      {
        LOGC("No input\r\n");
      }

      Input_Clear();
      inputMode = CALM_INMODE_NAV;   
    }

    /* Backspace Key - Decrement Input Counter */
    else if ( ch == BACKSPACE_KEY || ch == DELETE_KEY )
    {
      Input_Delete_Char();
      Calm_Refresh_On_Next();
    }

    else if ( ch != UP_KEY && ch != DOWN_KEY && ch != LEFT_KEY && ch != RIGHT_KEY )
    {
      Input_Add_Char(ch);
      Calm_Refresh_On_Next();
    }
  }
  /* Navigation menu */
  else if ( inputMode == CALM_INMODE_NAV )
  {
  /* First character received */
    if ( esc_cnt == 0 )
    {
    /* Check for custom mapped character */
      uint8_t ii = 0;
      for ( ii = 0; ii < keyMapCount; ii++ )
      {
        if ( ch == keyMap[ii].key)
        {
          LOGV( "Custom key pressed\r\n");
          keyMap[ii].cbFunc();
          return;
        }
      }

    /* Return Key - Select menu option */
      if ( ch == ENTER_KEY )
      {
        Cli_Navigate(CLI_NAV_SELECT);
        Calm_Refresh_On_Next();
      }

    /* Escape Key - Ignore Next Char */
      else if (ch == ESCAPE_KEY)
      {
        esc_cnt = 1;
      }

    /* CTRL-C - Clear screen */
      else if (ch == CLEAR_KEY)
      {
        Calm_CLI_Log_Clear();
      }

    /* Space Key - Pause Display */
      else if ( ch == SPACE_KEY && !inputMode )
      {
        Toggle_Refresh_Enable();
      }

    /* Minux Key - Decrease logging verbosity */
      else if (ch == MINUS_KEY)
      {
        Calm_Adjust_Level(-1);
      }

    /* Plus Key (actually '=') - Increase logging verbosity */
      else if (ch == PLUS_KEY)
      {
        Calm_Adjust_Level(1);
      }

    /* Other character - Store */
      else
      {
        LOGV( "[CALM] Unknown character received: %c / 0x%x)\r\n", (char)ch, ch);
      }
    }

  /* Ignore character received after escape */
    else if (esc_cnt == 1)
    {
      esc_cnt = 2;
    }

  /* Capture special character */
    else if (esc_cnt == 2)
    {
    /* Special character done */
      esc_cnt = 0;

    /* Up, Down, Left Key */
      if (ch == UP_KEY)
      {
        Cli_Navigate(CLI_NAV_UP);
      }
      else if (ch == DOWN_KEY)
      {
        Cli_Navigate(CLI_NAV_DOWN);
      }
      else if (ch == LEFT_KEY)
      {
        Cli_Navigate(CLI_NAV_BACK);
      }
      else if (ch == RIGHT_KEY)
      {
        Cli_Navigate(CLI_NAV_SELECT);
      }
      Calm_Refresh_On_Next();
    }
  }
}

/**
  * @brief  Handles input in command mode
  * @param  Character being handled
  * @retval None
  */
static void Handle_Command_Input ( uint8_t ch )
{
  /* First byte of command is length */
  if ( cliRxCnt == 0 )
  {
    cliRxLen = ch + 1;
  }
  
  Input_Add_Char(ch);

  /* All bytes received */
  if ( cliRxCnt == cliRxLen )
  {
    if ( cmdCbFunc != NULL )
    {
      cmdCbFunc(cliRxBuff, cliRxCnt);
    }
    else
    {
      LOGE( "No command callback registered\r\n");
    }
    /* Signal that a command has been fully read */
    Input_Clear();
  }   
}

/**
  * @brief  Handle log mode inputs
  * @param  ch received character
  * @retval None
  */
static void Handle_Log_Input ( uint8_t ch )
{
  /* Custom Keymap */
  uint8_t ii = 0;
  for ( ii = 0; ii < keyMapCount; ii++ )
  {
    if ( ch == keyMap[ii].key)
    {
      LOGV( "Custom key pressed\r\n");
      keyMap[ii].cbFunc();
      return;
    }
  }

  /* CTRL-C - Clear screen */
  if (ch == CLEAR_KEY)
  {
    Calm_CLI_Log_Clear();
    Calm_Refresh_On_Next();
  }

  /* Space Key - Refresh Display */
  else if ( ch == SPACE_KEY )
  {
    Toggle_Refresh_Enable();
  }

  /* Minux Key - Decrease logging verbosity */
  else if (ch == MINUS_KEY)
  {
    Calm_Adjust_Level(-1);
  }

  /* Plus Key (actually '=') - Increase logging verbosity */
  else if (ch == PLUS_KEY)
  {
    Calm_Adjust_Level(1);
  }

  /* Other character - Store */
  else
  {
    LOGV( "[CALM] Unknown character received: %c / 0x%x)\r\n", (char)ch, ch);
  }
}

/**
  * @brief  State switch for handling received input
  * @param  ch received character
  * @retval None
  */
static void Handle_Input ( uint8_t ch )
{
  switch ( mode )
  {
    case CALM_MODE_CLI:
      Handle_CLI_Input(ch);
      break;

    case CALM_MODE_CMD:
      Handle_Command_Input(ch);
      break;

    case CALM_MODE_LOG:
      Handle_Log_Input(ch);
      break;

    default:
      break;
  }
}

/**
  * @brief  User input prompt
  * @param  None
  * @retval None
  */
static void Prompt_Input ( void )
{
  LOGP("Input Value:\r\n\r\n > ");
  LOGP("%s\r\n\r\n", cliRxBuff);
}


/**
  * @brief  Parse data received over UART
  * @details Parses a CSV buffer until termination character is reached
  * @param  rxByte Pointer to buffer containing data received from UART interface
  * @retval None
  */
static void Input_Parse ( uint8_t * cliRxBuff, uint8_t len )
{
  uint8_t ii = 0; /* Iterate each byte of user input buffer */
  uint8_t jj = 0; /* Accumulate string buffer */
  char sBuff[10]; /* String buffer - stores user input decimal integer as a string */

  /* Loop until termination character */
  for (ii = 0; ii < len; ii++)
  {
    /* Comma separator reached. Read data buffered and go to next entry */
    if (cliRxBuff[ii] == ',')
    {
#if (CLI_INPUT_MODE == CLI_INPUT_DECIMAL)
      cliInputBuff[cliInputLen++] = atoi(sBuff);
#elif (CLI_INPUT_MODE == CLI_INPUT_HEX)
      cliInputBuff[cliInputLen++] = strtol(sBuff, NULL, 16);
#endif
      memset(sBuff, 0, sizeof(sBuff));
      jj = 0;
    }
    /* Ignore space characters */
    else if (cliRxBuff[ii] == ' ')
    {
    }
    /* Concat ASCII of number received to string buffer */
    else
    {
      sBuff[jj++] = (char)cliRxBuff[ii];
    }
  }

  /* Display received input */
  Calm_Print_Buffer(LOG_INFO, cliInputBuff, cliInputLen, "User Input: ");
}




/************************************************************************
 *********************** Initialization Functions ***********************
 ************************************************************************/

/**
  * @brief  Register a key press to a callback function
  * @param  key    Key press to map
  * @param  cbFunc Function to call on key press
  * @retval None
  */
bool Calm_Register_Key ( uint8_t key, keymapCB_t cbFunc )
{
  if ( keyMapCount < KEY_MAP_MAX )
  {
    keyMap[keyMapCount].key    = key;
    keyMap[keyMapCount].cbFunc = cbFunc;
    keyMapCount++; 
    return true;
  }
  else
  {
    return false;
  }
}

/**
  * @brief  DeInitialization function
  * @param  None
  * @retval None
  */
void Calm_DeInitialize ( void )
{
  queueDeInitialize(hLogQueue);
  queueDeInitialize(hRxQueue);
}

/**
  * @brief  Initialization function
  * @param  init Initialization parameters
  * @retval None
  */
int Calm_Initialize ( calm_init_t init )
{
  mode = init.mode;
  if ( init.scrollback != 0 )
  {
    scrollback = init.scrollback;
  }
  
  if ( init.lineSize != 0 )
  {
    lineSize = init.lineSize;
  }

  if ( init.uartBuffSize != 0 )
  {
    uartBuffSize = init.uartBuffSize;
  }

  sysLogLevel = init.logLevel;
  timeFunc = init.timeFunc;

  /* Initialize queues */
  hRxQueue  = queueInitialize(uartBuffSize, 1, false);
  if ( hRxQueue == NULL )
  {
    return -1;
  }

  /* State switch for initializing each mode */
  switch (mode)
  {
    case CALM_MODE_CLI:
      /* Initialize queues */
      hLogQueue = queueInitialize(scrollback, lineSize, true);
      if ( hLogQueue == NULL )
      {
        return -1;
      }

      if ( init.period != 0 )
      {
        refreshPer = init.period;
      }
      CLI_Initialize(init.menu, Calm_Cli_Input_Req);
      timestamp = timeFunc();
      break;

   case CALM_MODE_CMD:
      cmdCbFunc = init.cmdFunc;
      break;
      
    case CALM_MODE_LOG:
      timestamp = timeFunc();
      break;

    default:
      break;
  }
  calmInitialized = true;

  Clear_Screen();
  return 0;
}

