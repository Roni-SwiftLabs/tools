/**
 * @file cli.h
 * @author Daniel Bujak
 * @date August 20, 2018
 * @brief Command line interface definitions and function prototypes
 *
 */

/* Define to prevent recursive inclusion -------------------------------------*/
#ifndef __CLI_H
#define __CLI_H

#ifdef __cplusplus
 extern "C" {
#endif

#include <stdint.h>
#include <stdbool.h>

#define CLI_MSG_MAX_LEN        100          /**< Maximum number of bytes on input buffer */
#define CLI_MAX_MENUS          15           /**< Maximum number of menus */
#define CLI_MAX_INPUT_LEN      20           /**< Maximum length of parsed user input array */

/* Format of user input */
#define CLI_INPUT_DECIMAL      0
#define CLI_INPUT_HEX          1
#define CLI_INPUT_MODE         CLI_INPUT_HEX

/* Character Definitions */
#define CLEAR_KEY              3   /**< ctrl-c */
#define BACKSPACE_KEY          10
#define ENTER_KEY              13
#define CARRIAGE_RETURN        15
#define COMMAND_KEY            24  /**< ctrl-x */
#define ESCAPE_KEY             27
#define SPACE_KEY              32
#define MINUS_KEY              45
#define ZERO_KEY               48
#define NINE_KEY               57
#define PLUS_KEY               61  /**< Actually the equal key */
#define UP_KEY                 65
#define DOWN_KEY               66
#define RIGHT_KEY              67
#define LEFT_KEY               68
#define PRE_CMD_KEY            91  /**< Putty sends this to indicate arrow key */
#define DELETE_KEY             127


/* Declaration of CLI function callback type */
typedef void ( *cli_cb_t )( uint8_t * data, uint8_t len );

/* Declaration of CLI input request callback type */
typedef void ( *cli_input_cb_t )( void );


/**
 * @brief Command line interface navigation options
 *
 * Identifies the menu type as either a a sub-menu, an information display page,
 * or user input
 */
typedef enum
{
  CLI_NAV_SELECT = 0,                   /**< Select current entry */
  CLI_NAV_UP,                           /**< Navigate Up */
  CLI_NAV_DOWN,                         /**< Navigate Down */
  CLI_NAV_BACK,                         /**< Navigate Back */
  CLI_NAV_CNT
} cli_nav_t;


/**
 * @brief Menu type
 *
 * Identifies the menu type as either a a sub-menu, an information display page,
 * or user input
 */
typedef enum
{
  CLI_MENU_TYPE_ENTRIES = 0,            /**< Sub-Menu entry */
  CLI_MENU_TYPE_INFO,                   /**< Information display entry */
  CLI_MENU_TYPE_INPUT                   /**< User input entry */
} cli_menu_type_t;


/**
 * @brief Menu entry
 *
 * Each menu entry in the CLI is identified by this type
 */
typedef struct cli_menu_entry
{
  char               string[32];        /**< String to display */
  uint8_t            menuDest;          /**< Destination menu (on selection) */
  cli_cb_t           cbFunc;            /**< Function pointer to call (on selection) */
} cli_menu_entry_t;


/**
 * @brief Cli Menu Class
 *
 * Each menu in the CLI is identified by this type
 */
typedef struct cli_menu
{
  char               header[64];        /**< Menu title */
  cli_menu_type_t    menuType;          /**< Menu type */
  uint8_t            numEntries;        /**< Number of entries in the menu */
  cli_menu_entry_t  *pEntries;          /**< Pointer to the first entry */
  void             (*setupFunc)(void);  /**< Pointer to a function to be run upon entering the menu */
} cli_menu_t;


/* Function Prototypes */
void            CLI_Initialize    ( cli_menu_t ** menus, cli_input_cb_t cb );
void            Cli_Navigate      ( cli_nav_t cmd );
void            CLI_Redraw        ( void );
void            CLI_Input_Ready   ( uint8_t * data, uint8_t len );
cli_menu_type_t CLI_Menu_Type_Get ( void );

#ifdef __cplusplus
}
#endif
#endif /* __CLI_H */

