#include "cli.h"
#include "menu.h"
#include "calm.h"
#include "command.h"

#include <stdio.h>


void Command_Handler( uint8_t * data, uint8_t len)
{
  cmd_test_t cmd = (cmd_test_t) * (data+1);

  for (int ii = 0; ii < len ; ii ++)
  {
    LOGCMD("0x%x ", data[ii]);
  }
  switch ( cmd )
  {
    case CMD_SET_BATT1:
      LOGCMD("\r\nBattery 1 update command received\r\n");
      break;

    case CMD_SET_BATT2:
      LOGCMD("\r\nBattery 2 update command received\r\n");
      break;

    case CMD_TEST_SET_BATT:
      LOGCMD("\r\nBattery set command received\r\n");
      break;

    default:
      LOGCMD("\r\nUnknown command received\r\n");
      break; 
  }
}


