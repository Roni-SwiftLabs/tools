

#ifndef __CALM_TEST_H
#define __CALM_TEST_H

#define TEST_COUNT 5
#define REGISTER_KEY 0x60 // ` key

void Main_Init(calm_mode_t mode);

uint32_t GetTick(void);



#endif
