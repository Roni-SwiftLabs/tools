
#ifndef __COMMAND_H
#define __COMMAND_H

#include "cli.h"

#include <stdint.h>

typedef enum
{
  CMD_SET_BATT1 = 0x10,
  CMD_SET_BATT2 = 0x20,
  CMD_TEST_SET_BATT = 0x30,
  CMD_TEST_CNT
} cmd_test_t;

void Command_Handler( uint8_t * data, uint8_t len);
void Command_Initialize( void );
void Command_Main( void );

#endif