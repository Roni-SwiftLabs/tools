/**
 * @file cli.c
 * @author Daniel Bujak
 * @date August 20, 2018
 * @brief Command Line Interface module
 *
 * Provides a command line interface over the UART port to allow users to debug
 * and control the system during bring-up and testing
 *
 */

/* Includes ------------------------------------------------------------------*/
#include "cli.h"
#include "calm.h"

#include <stdlib.h>
#include <string.h>

/* CLI Library Variables */
static cli_menu_t **  pMenus;                            /* Pointer to menu list */
static cli_input_cb_t inputCb;

/* Status tracking variables */
uint8_t  activeMenu    = 0;
uint8_t  returnMenu    = 0;
uint8_t  cursorIndex   = 0;

/* Private functions */
static void Display_Cursor ( uint8_t r );



/**
  * @brief  Redraw the CLI display. If force is not set the display will only
  *         be updated on the System Status page. Otherwise if force is set the
  *         display will be updated
  * @param  force force update regardless of menu we're in
  * @retval None
  */
void CLI_Redraw ( void )
{
  cli_menu_t * menu = pMenus[activeMenu];

  uint8_t ii = 0;

  if (menu->menuType == CLI_MENU_TYPE_ENTRIES)
  {
    /* Display Title */
    LOGC("*** %s ***\r\n", menu->header);

    /* Display Entries on Screen */
    for (ii = 0; ii < menu->numEntries; ii++)
    {
      LOGC("    %s\r\n", menu->pEntries[ii].string);
    }

    /* Display Cursor */
    Display_Cursor(cursorIndex + 1);
  }

  else if (menu->menuType == CLI_MENU_TYPE_INFO)
  {
    /* Call setup function */
    if ( *menu->setupFunc != NULL )
    {
      (*menu->setupFunc)();
    }
    LOGC("\033[2B");
  }
}


/**
  * @brief  Process a navigation command
  * @param  cmd Navigation command received
  * @retval None
  */
void Cli_Navigate ( cli_nav_t cmd )
{
  /* If we're in a navigable menu */
  if (pMenus[activeMenu]->menuType == CLI_MENU_TYPE_ENTRIES)
  {
    switch (cmd)
    {
      /* Select current selection */
      case (CLI_NAV_SELECT):

        /* Call menu entry callback function unless menu type is input in which case we take the input first */
        if (pMenus[pMenus[activeMenu]->pEntries[cursorIndex].menuDest]->menuType != CLI_MENU_TYPE_INPUT)
        {
          pMenus[activeMenu]->pEntries[cursorIndex].cbFunc(NULL, 0);

          /* Update view variable and reset menu cursor if changing menus */
          if (activeMenu != pMenus[activeMenu]->pEntries[cursorIndex].menuDest)
          {
            /* Set return variable */
            returnMenu = activeMenu;
            activeMenu = pMenus[activeMenu]->pEntries[cursorIndex].menuDest;
            cursorIndex = 0;
          }
        }

        /* If destination is input type, request input */
        else  
        {
          inputCb();
        }
        break;

      /* Navigate up in menu */
      case (CLI_NAV_UP):
        if (cursorIndex > 0)
        {
          cursorIndex--;
        }
        else
        {
          cursorIndex = pMenus[activeMenu]->numEntries - 1;
        }
        break;

      /* Navigate down in menu */
      case (CLI_NAV_DOWN):
        if (cursorIndex < pMenus[activeMenu]->numEntries - 1)
        {
          cursorIndex++;
        }
        else
        {
          cursorIndex = 0;
        }
        break;

      /* Navigate back in menu */
      case (CLI_NAV_BACK):
        activeMenu = returnMenu;
        cursorIndex = 0;
        break;

      default:
        break;
    }
  }

  /* Any nav cmd rx'ed in info display only type menu causes return to last menu */
  else if (pMenus[activeMenu]->menuType == CLI_MENU_TYPE_INFO)
  {
    activeMenu = returnMenu;
    returnMenu   = 0;  /* Set next return to home as we only have a single level of history */
    cursorIndex = 0;
  }
}


/**
  * @brief  Accepts input data
  * @param  *data Input data buffer
  * @retval None
  */
void CLI_Input_Ready ( uint8_t * data, uint8_t len )
{
  pMenus[activeMenu]->pEntries[cursorIndex].cbFunc(data, len);
}


/**
  * @brief  Returns active menu type
  * @param  None
  * @retval Active menu type
  */
cli_menu_type_t CLI_Menu_Type_Get ( void )
{
  return pMenus[activeMenu]->menuType;
}


/**
  * @brief  Cancels input data
  * @param  None
  * @retval None
  */
void CLI_Input_Cancel ( void )
{
  //
}


/**
  * @brief  Display the cursor on the UART terminal
  * @param  r The cursor will appear at the start of this row
  * @retval None
  */
static void Display_Cursor ( uint8_t r )
{
  LOGC("\033[0;0H");        // Cursor Home
  LOGC("\033[%dB", r);      // Cursor down 'r' rows
  LOGC("  >");
  LOGC("\033[0;0H");        // Cursor Home

  /* Move Cursor down enough rows to clear the biggest menu */
  LOGC("\033[%dB", (pMenus[activeMenu]->numEntries + 2));
}


/**
  * @brief  Initialization of the CLI system
  * @param  ** menuList Pointer to list of menus
  * @param  cb          Callback function for input request
  * @retval None
  */
void CLI_Initialize ( cli_menu_t ** menuList, cli_input_cb_t cb )
{
  pMenus = menuList;
  inputCb = cb;
}

