/**
 * @file timeout.h
 * @author Daniel Bujak
 * @date September 19, 2018
 * @brief Timeout module related definitions
 *
 */

/* Define to prevent recursive inclusion -------------------------------------*/
#ifndef __TIMEOUT_H
#define __TIMEOUT_H

#ifdef __cplusplus
 extern "C" {
#endif

#include <stdbool.h>
#include <stdint.h>

#define NUM_TO_SLOTS         10       /**< Number of timeout slots available */

/* Forward declaration of callback type */
typedef void ( *timeout_cb_t ) ( uint32_t data );


/**
 * @brief Timer module error codes
 */
typedef enum
{
  TIMEOUT_ERR_SUCCESS = 0,      /**< No error */
  TIMEOUT_ERR_FULL              /**< Timeout list full error */
} timeout_error_t;

/**
 * @brief List of modules supporting timeout function
 */
typedef enum
{
  TO_MODE_NONE  = 0,
  TO_MOD_SYSTEM,            /**< Main system timeout */
  TO_MOD_DAKOTA,            /**< Dakota system timeout */
  TO_MOD_SATCOM,            /**< Satcom related timeouts */
  TO_MOD_GENERIC_CB         /**< Generic callback function */
} timeout_modules_t;


/**
 * @brief Timeout Monitor Structure
 */
typedef struct
{
  bool                used;      /**< Indicates whether this slot is active */
  timeout_modules_t   type;      /**< Indicates which module the slot is for */
  timeout_cb_t        cb;        /**< Callback function in event of timeout */
  uint32_t            to_ticks;  /**< Duration of timeout in timer ticks */
  uint8_t             id;        /**< Timeout id*/
  uint32_t            data;      /**< Data passed to callback*/
} timeout_t;


/* Function Prototypes */
void            Timeout_Initialize       ( uint16_t timeoutFreq );
void            Timeout_Deinitialize     ( void );
void            Timeout_Handle_Interrupt ( void );
int8_t          Timeout_Add              ( timeout_cb_t cb, uint32_t to_ms, uint32_t data );
void            Timeout_Del_Cb           ( timeout_cb_t cb );
void            Timeout_Del_Cb_Data      ( timeout_cb_t cb, uint32_t data );
void            Timeout_Del_Id           ( uint8_t id );

#ifdef __cplusplus
}
#endif
#endif /* __TIMEOUT_H */
