/**
 * @file timeout.c
 * @author Daniel Bujak
 * @date September 19, 2018
 * @brief Timeout module
 */

/* Includes ------------------------------------------------------------------*/
#include "timeout.h"

#include <stdint.h>

/* Local Variables */
static timeout_t timeoutList[NUM_TO_SLOTS];     
static uint16_t  timeoutHz = 0;
static bool      timeoutEn = false;
static uint8_t   timeoutID = 0;

/**
  * @brief  Removes a timeout timer specified by callback function and callback data
  * @details Parses the list for the specified timeout and removes it if it exists
  * @param cb Callback function to match
  * @param data Callback data to match
  */
void Timeout_Del_Cb_Data ( timeout_cb_t cb, uint32_t data )
{
  uint8_t ii = 0;

  /* Check each timeout slot */
  for (ii = 0; ii < NUM_TO_SLOTS; ii++)
  {
    if ( timeoutList[ii].cb == cb && timeoutList[ii].data == data)
    {
      timeoutList[ii].used = false;
    }
  }
}

/**
  * @brief  Removes a timeout timer specified by callback function
  * @details Parses the list for the specified timeout and removes it if it exists
  * @param cb Callback function to match
  */
void Timeout_Del_Cb ( timeout_cb_t cb )
{
  uint8_t ii = 0;
  /* Check each timeout slot */
  for (ii = 0; ii < NUM_TO_SLOTS; ii++)
  {
    if ( timeoutList[ii].cb == cb )
    {
      timeoutList[ii].used = false;
    }
  }
}

/**
  * @brief  Removes a timeout timer specified by id
  * @details Parses the list for the specified timeout and removes it if it exists
  * @param id Timeout identifier to match
  */
void Timeout_Del_Id ( uint8_t id )
{
  uint8_t ii = 0;

  /* Check each timeout slot */
  for (ii = 0; ii < NUM_TO_SLOTS; ii++)
  {
    if ( timeoutList[ii].id == id )
    {
      timeoutList[ii].used = false;
    }
  }
}


/**
  * @brief  Add a timeout timer with a callback
  *
  * @details Checks to see if there are any available slots in the timeout timer list
  *          and adds the specified timer to the list if there is.
  *          NOTE: The minumum timeout is (1/HF_EVENT_HZ)
  * @param  cb : Pointer to function to callback to
  * @param  to_ms : Timeout period in ms
  * @param  module : The module the timeout flag should be set in
  *
  * @retval error code
  */
int8_t Timeout_Add ( timeout_cb_t cb, uint32_t to_ms, uint32_t data )
{
  timeout_t entry;

  entry.used     = true;
  entry.type     = TO_MOD_GENERIC_CB;
  entry.cb       = cb;
  entry.to_ticks = ( (to_ms * timeoutHz) / 1000 ) - 1;
  entry.data     = data;

  uint8_t ii = 0;

  /* Check if we have a free slot */
  for ( ii = 0; ii < NUM_TO_SLOTS; ii++ )
  {
    if (timeoutList[ii].used == false)
    {
      break;
    }
  }

  /* Empty slot was found - add timeout here */
  if (ii < NUM_TO_SLOTS)
  {
    timeoutList[ii].used     = true;
    timeoutList[ii].type     = entry.type;
    timeoutList[ii].to_ticks = entry.to_ticks;
    timeoutList[ii].cb       = entry.cb;
    timeoutList[ii].id       = timeoutID++;
    timeoutList[ii].data     = entry.data;

    /* Don't allow ticks to be 0 */
    if (timeoutList[ii].to_ticks == 0)
    {
      timeoutList[ii].to_ticks = 1;
    }
    return timeoutList[ii].id;
  }
  /* No empty slots available */
  else
  {
    return -1;
  }
}


/**
  * @brief  Monitors the timeout timers
  *
  * @details Checks to see if any of the timeout timers have expired, and if so sets
  *          the flag specified by the timer and deletes the timeout
  */
void Timeout_Handle_Interrupt ( void )
{
  if ( timeoutEn )
  {
    uint8_t ii = 0;

    /* Check each timeout slot */
    for (ii = 0; ii < NUM_TO_SLOTS; ii++)
    {
      /* If slot is used then process it */
      if (timeoutList[ii].used == true)
      {
        /* If timeout expired set the appropriate flag and free up slot */
        if (timeoutList[ii].to_ticks == 0)
        {
          timeoutList[ii].used = false;
          timeoutList[ii].cb(timeoutList[ii].data);
        }
        else
        {
        /* Decrement counter */
          timeoutList[ii].to_ticks--;
        }
      }
    }
  }
}


/**
  * @brief  Clear all entried in timeout queue
  * @param  None
  * @retval None
  */
static void Timeout_Clear_All ( void )
{
  uint8_t ii = 0;

  /* Free up all slots */
  for ( ii = 0; ii < NUM_TO_SLOTS; ii++ )
  {
    timeoutList[ii].used = false;
  }
}


/**
  * @brief  Timeout timer initialization function
  * @param  timeoutFreq Frequency at which handler will be called by application
  * @retval None
  */
void Timeout_Initialize ( uint16_t timeoutFreq )
{
  Timeout_Deinitialize();
  timeoutHz = timeoutFreq;
  timeoutEn = true;
}


/**
  * @brief  Timeout timer deinitialization function
  * @param  None
  * @retval None
  */
void Timeout_Deinitialize ( void )
{
  timeoutID = 0;
  timeoutEn = false;
  Timeout_Clear_All();
}

