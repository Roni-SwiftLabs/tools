#include "timeout_test.h"
#include "timeout.h"

#include <stdio.h>
#include <stdlib.h>
#include <stdint.h>
#include <pthread.h>
#include <time.h>
#include <unistd.h>

#define TIMEOUT_PERIOD1    1000
#define TIMEOUT_PERIOD2    2000
#define TIMEOUT_PERIOD3    1500
#define TIMEOUT_FREQ       1000
#define TIMEOUT_PER_US     1000000/TIMEOUT_FREQ

void *mainThread             ( void *vargp );
void  Callback1_Function     ( uint32_t data );  
uint32_t timedifference_usec ( struct timespec t0, struct timespec t1 );
static uint32_t get_usec     (struct timespec t);

bool deleted = false;
bool refresh = false;
uint32_t tick = 0;
struct timespec stop, start, delay1, delay2;

timeout_cb_t cb1 = &Callback1_Function;
//timeout_cb_t cb2 = &Callback2_Function;
//timeout_cb_t cb3 = &Callback3_Function;

uint32_t task1Last_us = 0;
uint16_t count = 0;

uint32_t t0_us; 

//uint32_t cb_us[NUM_FUN];
uint32_t cb1_us = 0;

static uint32_t dataResult = 0;

void Callback1_Function( uint32_t data )
{
  struct timespec t;
  clock_gettime(CLOCK_MONOTONIC, &t);
  cb1_us = get_usec(t);
  dataResult += data;
}


uint32_t timedifference_usec(struct timespec t0, struct timespec t1)
{
  return (t1.tv_sec - t0.tv_sec) * 1000000 + (t1.tv_nsec - t0.tv_nsec) / 1000;
}


static uint32_t get_usec(struct timespec t)
{
  return (t.tv_sec * 1000000) + (t.tv_nsec / 1000 );
}

static bool test_delete_cb ( void )
{
  Timeout_Initialize(TIMEOUT_FREQ);

  struct timespec t0;
  clock_gettime(CLOCK_MONOTONIC, &t0);
  uint32_t start_us = get_usec(t0);
  cb1_us = start_us;

  Timeout_Add(cb1, 1000, 1);  /* Add timeout for 1000ms */
  Timeout_Add(cb1, 1000, 2);  /* Add timeout for 1000ms */
  Timeout_Add(cb1, 1000, 3);  /* Add timeout for 1000ms */
  usleep(500000);             /* Sleep 500ms */
  Timeout_Del_Cb(cb1);        /* Delete timeout */
  usleep(1000000);            /* Sleep 1000ms */

  Timeout_Deinitialize();

  return ( cb1_us == start_us );
}


static bool test_delete_cb_data ( void )
{
  Timeout_Initialize(TIMEOUT_FREQ);

  struct timespec t0;
  clock_gettime(CLOCK_MONOTONIC, &t0);

  uint32_t start_us = get_usec(t0);
  cb1_us = start_us;
  
  uint32_t data1 = 1;
  uint32_t data2 = 2;
  dataResult = 0;
  
  Timeout_Add(cb1, 1000, data1);  /* Add timeout for 1000ms */
  Timeout_Add(cb1, 1000, data2);  /* Add timeout for 1000ms */
  usleep(500000);                 /* Sleep 500ms */
  Timeout_Del_Cb_Data(cb1, data1);    /* Delete timeout */
  usleep(1000000);                /* Sleep 1000ms */

  Timeout_Deinitialize();

  return ( dataResult == data2 );
}


static bool test_delete_id ( void )
{
  Timeout_Initialize(TIMEOUT_FREQ);

  struct timespec t0;
  clock_gettime(CLOCK_MONOTONIC, &t0);
  uint32_t start_us = get_usec(t0);
  cb1_us = start_us;
  uint32_t data = 1;
  dataResult = 0;
  uint8_t id1;

  id1 = Timeout_Add(cb1, 1000, data);  /* Add timeout for 1000ms */
  Timeout_Add(cb1, 1000, data);        /* Add timeout for 1000ms */
  Timeout_Add(cb1, 1000, data);        /* Add timeout for 1000ms */
  usleep(500000);                      /* Sleep 500ms */
  Timeout_Del_Id(id1);                 /* Delete timeout */
  usleep(1000000);                     /* Sleep 1000ms */

  Timeout_Deinitialize();
  return ( dataResult == 2 );
}

static bool test_id ( void )
{
  Timeout_Initialize(TIMEOUT_FREQ);

  struct timespec t0;
  clock_gettime(CLOCK_MONOTONIC, &t0);
  uint32_t start_us = get_usec(t0);
  cb1_us = start_us;
  uint32_t data = 1;
  dataResult = 0;
  uint8_t id1, id4;

  id1 = Timeout_Add(cb1, 1000, data);  /* Add timeout for 1000ms */
  Timeout_Add(cb1, 1000, data);        /* Add timeout for 1000ms */
  Timeout_Add(cb1, 1000, data);        /* Add timeout for 1000ms */
  usleep(500000);                      /* Sleep 500ms */
  Timeout_Del_Id(id1);                 /* Delete timeout */
  usleep(1000000);                     /* Sleep 1000ms */
  id4 = Timeout_Add(cb1, 1000, data);
  Timeout_Deinitialize();
  return ( id4 == id1 + 3 );
}

static bool test_add ( void )
{
  Timeout_Initialize(TIMEOUT_FREQ);

  struct timespec t0;
  clock_gettime(CLOCK_MONOTONIC, &t0);
  uint32_t start_us = get_usec(t0);
 
  Timeout_Add(cb1, 1000, 2);   /* Add timeout for 1000ms */
  usleep(1000000);          /* Sleep 1000ms */

  Timeout_Deinitialize();

  return ( cb1_us >= start_us + 995000 );
}

static bool test_full ( void )
{
  bool full = false;
  Timeout_Initialize(TIMEOUT_FREQ);
  for (uint8_t ii = 0; ii < NUM_TO_SLOTS + 1; ii ++)
  {
    if ( Timeout_Add(cb1, 1000, 3) == -1 )
    {
      full = true;
    }
  }
  return full;
}

static bool test_clear ( void )
{
  bool full = false;
  Timeout_Initialize(TIMEOUT_FREQ);

  Timeout_Add(cb1, 1000, 4);
  
  Timeout_Deinitialize();
  
  Timeout_Initialize(TIMEOUT_FREQ);

  for (uint8_t ii = 0; ii < NUM_TO_SLOTS ; ii ++)
  {
    if (Timeout_Add(cb1, 1000, 5) == -1)
    {
      full = true;
    }
  }
  return !full;
}


static bool test_clear2 ( void )
{
  bool full = false;
  Timeout_Initialize(TIMEOUT_FREQ);

  Timeout_Add(cb1, 1000, 6);
  
  Timeout_Del_Cb(cb1);
  
  Timeout_Initialize(TIMEOUT_FREQ);

  for (uint8_t ii = 0; ii < NUM_TO_SLOTS ; ii ++)
  {
    if (Timeout_Add(cb1, 1000, 7) == -1)
    {
      full = true;
    }
  }
  return !full;
}


void *mainThread( void *vargp )
{
  struct timespec t0, tNow;
  uint32_t tNext_us, tNow_us;

  clock_gettime(CLOCK_MONOTONIC, &t0);
  t0_us = get_usec(t0);

  tNext_us = t0_us + TIMEOUT_PER_US;

  while (1)
  {
    clock_gettime(CLOCK_MONOTONIC, &tNow);
    tNow_us = get_usec(tNow);

    if ( tNow_us >= tNext_us )
    {
      tNext_us += TIMEOUT_PER_US;
      Timeout_Handle_Interrupt();
    }
  }
}

int main (int argc, char *argv[])
{ 
  pthread_t main_id;
  pthread_create(&main_id, NULL, mainThread, NULL);

  printf("Starting Timeout Test App (Period = %dus)\r\n", TIMEOUT_PER_US);
  printf("Delete test cb:            %d \r\n", test_delete_cb());
  printf("Delete test cb and data:   %d \r\n", test_delete_cb_data());
  printf("Delete test id:            %d \r\n", test_delete_id());
  printf("Id test:                   %d \r\n", test_id());
  printf("Add test:                  %d \r\n", test_add());
  printf("Full test:                 %d \r\n", test_full());
  printf("Clear test:                %d \r\n", test_clear());
  printf("Clear test 2:              %d \r\n", test_clear2());
  printf("##### Testing complete #####\r\n");
}


