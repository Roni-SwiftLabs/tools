/**
 * @file command.h
 * @author Daniel Bujak
 * @date August 28, 2018
 * @brief Commanding interface module definitions
 *
 */

/* Define to prevent recursive inclusion -------------------------------------*/
#ifndef __COMMAND_H
#define __COMMAND_H

#ifdef __cplusplus
 extern "C" {
#endif

#include <stdint.h>

void Command_Handler ( uint8_t * data, uint8_t len);

/**
 * @brief Charging events
 */
typedef enum
{
  CMD_TEST_SET_BATT = 0x31,
  CMD_TEST_CNT
} cmd_test_t;


#ifdef __cplusplus
}
#endif
#endif /* __COMMAND_H */
