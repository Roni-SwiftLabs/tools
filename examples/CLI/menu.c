/**
 * @file menu.c
 * @author Daniel Bujak
 * @date August 23, 2018
 * @brief Command line interface menu contents
 *
 */

/* Includes ------------------------------------------------------------------*/
#include "app_config.h"

#include "adc.h"
#include "ask.h"
#include "bq28z610.h"
#include "charger.h"
#include "cli.h"
#include "menu.h"
#include "dakota.h"
#include "fuel_gauge.h"
#include "calm.h"
#include "lt3650.h"
#include "power.h"
#include "satcom.h"
#include "strings.h"
#include "timer.h"
#include "usart.h"

#include <stdlib.h>

#ifndef UNUSED
#define UNUSED(x) (void)(x)
#endif

/* CLI Library Variables */
cli_menu_t * menuEntries[CLI_MENU_COUNT];

/* CLI Functions */
static void No_Action           ( uint8_t * d );

/* System control functions */
static void CLI_SystemSleep     ( uint8_t * d );
static void CLI_SystemIdle      ( uint8_t * d ); 
static void CLI_SystemWifi      ( uint8_t * d );
static void CLI_SystemSatcom    ( uint8_t * d );
static void CLI_SystemBootload  ( uint8_t * d );

/* Power Rail Functions */
static void vWifi_Set           ( uint8_t * d );
static void vSatcom_Set         ( uint8_t * d );
static void vUSB_Set            ( uint8_t * d );
static void vBoost_Set          ( uint8_t * d );

/* Dakota Functions */
static void Dakota_Reset_Toggle ( uint8_t * d );
static void Dakota_Tx_Push      ( uint8_t * d );
static void Dakota_Queue_Reset  ( uint8_t * d );

/* Battery Functions */
static void Battery_1_Toggle    ( uint8_t * d );
static void Battery_2_Toggle    ( uint8_t * d );
static void Battery_Ext_Toggle  ( uint8_t * d );
static void Charge_Select       ( uint8_t * d );
static void Charge_Set          ( uint8_t * d );
static void Charge_LB_Set       ( uint8_t * d );
static void FuelGauge_Info      ( uint8_t * d );
static void FuelGauge_Refresh   ( uint8_t * d );
static void FuelGauge_StdRead   ( uint8_t * d );
static void FuelGauge_MacRead   ( uint8_t * d );
static void FuelGauge_MacWrite  ( uint8_t * d );
static void Battery_Override    ( uint8_t * d );

/* ASK Functions */
static void Ask_Set_Power       ( uint8_t * d );
static void Ask_Cmd_Debug       ( uint8_t * d );

/* Status Display Functions */
static void CLI_Display_Help        ( void );
static void CLI_Print_Battery_Gauge ( void );


/* --------- CLI Library Menu Entries --------- */

/* Main Menu Entries */
cli_menu_entry_t entriesMain[] = {
  {.string = ".",                   .menuDest = CLI_MENU_MAIN,     .cbFunc = No_Action},
  {.string = "System State",        .menuDest = CLI_MENU_CONTROL,  .cbFunc = No_Action},
  {.string = "Power Rails",         .menuDest = CLI_MENU_POWER,    .cbFunc = No_Action},
  {.string = "Battery",             .menuDest = CLI_MENU_BATTERY,  .cbFunc = No_Action},
  {.string = "ASK",                 .menuDest = CLI_MENU_ASK,      .cbFunc = No_Action},
  {.string = "Status",              .menuDest = CLI_MENU_STATUS,   .cbFunc = No_Action},
  {.string = "Help",                .menuDest = CLI_MENU_HELP,     .cbFunc = No_Action} };

/* Battery Select Entries */
cli_menu_entry_t entriesBattery[] = {
  {.string = "..",                  .menuDest = CLI_MENU_MAIN,     .cbFunc = No_Action},
  {.string = "Battery 1     [0,1]", .menuDest = CLI_MENU_INPUT,    .cbFunc = Battery_1_Toggle},
  {.string = "Battery 2     [0,1]", .menuDest = CLI_MENU_INPUT,    .cbFunc = Battery_2_Toggle},
  {.string = "Battery Ext   [0,1]", .menuDest = CLI_MENU_INPUT,    .cbFunc = Battery_Ext_Toggle},
  {.string = "Charge Select [0,1]", .menuDest = CLI_MENU_INPUT,    .cbFunc = Charge_Select},
  {.string = "Charge On     [0,1]", .menuDest = CLI_MENU_INPUT,    .cbFunc = Charge_Set},
  {.string = "LB Toggle     [0,1]", .menuDest = CLI_MENU_INPUT,    .cbFunc = Charge_LB_Set},
  {.string = "Fg Info",             .menuDest = CLI_MENU_BATTERY,  .cbFunc = FuelGauge_Info},
  {.string = "Fg Refresh",          .menuDest = CLI_MENU_BATTERY,  .cbFunc = FuelGauge_Refresh},
  {.string = "STD R [Batt,Addr,L]", .menuDest = CLI_MENU_INPUT,    .cbFunc = FuelGauge_StdRead},
  {.string = "MAC R [B,A1,A2,L]",   .menuDest = CLI_MENU_INPUT,    .cbFunc = FuelGauge_MacRead},
  {.string = "MAC W [B,A1,A2,L,D]", .menuDest = CLI_MENU_INPUT,    .cbFunc = FuelGauge_MacWrite},
  {.string = "Manual Mode",         .menuDest = CLI_MENU_BATTERY,  .cbFunc = Battery_Override}};

/* Control Entries */
cli_menu_entry_t entriesControl[] = {
  {.string = "..",                  .menuDest = CLI_MENU_MAIN,     .cbFunc = No_Action},
  {.string = "System Idle" ,        .menuDest = CLI_MENU_CONTROL,  .cbFunc = CLI_SystemIdle},
  {.string = "System Wifi" ,        .menuDest = CLI_MENU_CONTROL,  .cbFunc = CLI_SystemWifi},
  {.string = "System Satcom" ,      .menuDest = CLI_MENU_CONTROL,  .cbFunc = CLI_SystemSatcom},
  {.string = "System Sleep",        .menuDest = CLI_MENU_CONTROL,  .cbFunc = CLI_SystemSleep},
  {.string = "Bootloader",          .menuDest = CLI_MENU_CONTROL,  .cbFunc = CLI_SystemBootload}};

/* Dakota Enable Entries */
cli_menu_entry_t entriesDakota[] = {
  {.string = "..",                  .menuDest = CLI_MENU_MAIN,     .cbFunc = No_Action},
  {.string = "Set Reset",           .menuDest = CLI_MENU_DAKOTA,   .cbFunc = Dakota_Reset_Toggle},
  {.string = "Dakota Push",         .menuDest = CLI_MENU_INPUT,    .cbFunc = Dakota_Tx_Push},
  {.string = "Queue Reset",         .menuDest = CLI_MENU_DAKOTA,   .cbFunc = Dakota_Queue_Reset}};


/* ASK Modem Entries */
cli_menu_entry_t entriesAsk[] = {
  {.string = "..",                 .menuDest = CLI_MENU_MAIN,  .cbFunc = No_Action},
  {.string = "Modem Power Toggle", .menuDest = CLI_MENU_ASK,   .cbFunc = Ask_Set_Power},
  //{.string = "Modem Clock Toggle", .menuDest = CLI_MENU_ASK,   .cbFunc = CLI_FLAG_ASK_CLOCK},
  //{.string = "Continuous Toggle",  .menuDest = CLI_MENU_ASK,   .cbFunc = CLI_FLAG_ASK_CONT},
  //{.string = "ASK Reset",          .menuDest = CLI_MENU_ASK,   .cbFunc = CLI_FLAG_ASK_RESET},
  //{.string = "Get Antenna Info",   .menuDest = CLI_MENU_ASK,   .cbFunc = CLI_FLAG_ASK_ANT_INFO},
  //{.string = "Get Model Number",   .menuDest = CLI_MENU_ASK,   .cbFunc = CLI_FLAG_ASK_MODEL_NO},
  //{.string = "Get Serial Number",  .menuDest = CLI_MENU_ASK,   .cbFunc = CLI_FLAG_ASK_SERIAL_NO},
  //{.string = "Get MCU FW Version", .menuDest = CLI_MENU_ASK,   .cbFunc = CLI_FLAG_ASK_MCU_VER},
  //{.string = "Get Antenna Type",   .menuDest = CLI_MENU_ASK,   .cbFunc = CLI_FLAG_ASK_ANT_TYPE},
  //{.string = "Get ADC Data",       .menuDest = CLI_MENU_INPUT, .cbFunc = CLI_FLAG_ASK_ADC_DATA},
  //{.string = "Get DAC Data",       .menuDest = CLI_MENU_INPUT, .cbFunc = CLI_FLAG_ASK_DAC_DATA},
  //{.string = "Set DAC Data",       .menuDest = CLI_MENU_INPUT, .cbFunc = CLI_FLAG_ASK_DAC_WRITE},
  //{.string = "Get DAC Cal Stat",   .menuDest = CLI_MENU_ASK,   .cbFunc = CLI_FLAG_ASK_GET_DAC_CAL},
  //{.string = "Get Start DAC Cal",  .menuDest = CLI_MENU_INPUT, .cbFunc = CLI_FLAG_ASK_START_DAC_CAL},
  //{.string = "Get Stop DAC Cal",   .menuDest = CLI_MENU_ASK,   .cbFunc = CLI_FLAG_ASK_STOP_DAC_CAL},
  //{.string = "Set Angle",          .menuDest = CLI_MENU_INPUT, .cbFunc = CLI_FLAG_ASK_SET_ANGLE},
  //{.string = "Get PA Status",      .menuDest = CLI_MENU_ASK,   .cbFunc = CLI_FLAG_ASK_PA_STAT},
  //{.string = "Set PA gate",        .menuDest = CLI_MENU_INPUT, .cbFunc = CLI_FLAG_ASK_PA_GATE},
  //{.string = "Set PA detection",   .menuDest = CLI_MENU_INPUT, .cbFunc = CLI_FLAG_ASK_PA_DETECT},
  //{.string = "Set Clock Out",      .menuDest = CLI_MENU_INPUT, .cbFunc = CLI_FLAG_ASK_SET_CLOCK},
  //{.string = "Set Comp LUT",       .menuDest = CLI_MENU_INPUT, .cbFunc = CLI_FLAG_ASK_SET_COMP_LUT},
  //{.string = "BER Tx",             .menuDest = CLI_MENU_ASK,   .cbFunc = CLI_FLAG_ASK_BERT_TX},
  //{.string = "BER Rx",             .menuDest = CLI_MENU_ASK,   .cbFunc = CLI_FLAG_ASK_BERT_RX},
  {.string = "Debug",              .menuDest = CLI_MENU_ASK,   .cbFunc = Ask_Cmd_Debug}
};

/* Power Control Entries */
cli_menu_entry_t entriesPower[] = {
  {.string = "..",          .menuDest = CLI_MENU_MAIN,  .cbFunc = No_Action},
  {.string = "VWifi On",    .menuDest = CLI_MENU_POWER, .cbFunc = vWifi_Set},
  {.string = "VSatcom On",  .menuDest = CLI_MENU_POWER, .cbFunc = vSatcom_Set},
  {.string = "VUSB On",     .menuDest = CLI_MENU_POWER, .cbFunc = vUSB_Set},
  {.string = "VBoost On",   .menuDest = CLI_MENU_POWER, .cbFunc = vBoost_Set}
  };


/* --------- CLI Library Menu Structures --------- */

/* Main Menu Type */
cli_menu_t menuMain = {
    .header     = "Main (space=pause, ctrl-c=clear)",
    .menuType   = CLI_MENU_TYPE_ENTRIES,
    .numEntries = sizeof(entriesMain)/sizeof(cli_menu_entry_t),
    .pEntries   = entriesMain,
    .setupFunc  = NULL };

/* System Control Menu Type */
cli_menu_t menuControl = {
    .header     = "System Control",
    .menuType   = CLI_MENU_TYPE_ENTRIES,
    .numEntries = sizeof(entriesControl)/sizeof(cli_menu_entry_t),
    .pEntries   = entriesControl,
    .setupFunc  = NULL };

/* Battery Menu Type */
cli_menu_t menuBattery = {
    .header     = "Battery (BATT1 = 0, BATT2 = 1, Off = 0, On = 1)",
    .menuType   = CLI_MENU_TYPE_ENTRIES,
    .numEntries = sizeof(entriesBattery)/sizeof(cli_menu_entry_t),
    .pEntries   = entriesBattery,
    .setupFunc  = NULL };

/* Dakota Menu Type */
cli_menu_t menuDakota = {
    .header     = "Dakota Control",
    .menuType   = CLI_MENU_TYPE_ENTRIES,
    .numEntries = sizeof(entriesDakota)/sizeof(cli_menu_entry_t),
    .pEntries   = entriesDakota,
    .setupFunc  = NULL };

/* ASK Menu Type */
cli_menu_t menuAsk = {
    .header     = "ASK Modem",
    .menuType   = CLI_MENU_TYPE_ENTRIES,
    .numEntries = sizeof(entriesAsk)/sizeof(cli_menu_entry_t),
    .pEntries   = entriesAsk,
    .setupFunc  = NULL };

/* Power Menu Type */
cli_menu_t menuPower = {
    .header     = "Power Control",
    .menuType   = CLI_MENU_TYPE_ENTRIES,
    .numEntries = sizeof(entriesPower)/sizeof(cli_menu_entry_t),
    .pEntries   = entriesPower,
    .setupFunc  = NULL };


/* Help Menu */
cli_menu_t menuHelp = {
    .header     = "",
    .menuType   = CLI_MENU_TYPE_INFO,
    .numEntries = 0,
    .pEntries   = NULL,
    .setupFunc  = CLI_Display_Help };

/* System Status Menu */
cli_menu_t menuStatus = {
    .header     = "",
    .menuType   = CLI_MENU_TYPE_INFO,
    .numEntries = 0,
    .pEntries   = NULL,
    .setupFunc  = CLI_Display_Status };

/* Input Menu Type */
cli_menu_t menuInput = {
    .header     = "Input Value:",
    .menuType   = CLI_MENU_TYPE_INPUT,
    .numEntries = 0,
    .pEntries   = NULL,
    .setupFunc  = NULL };


/**
  * @brief  Initialize the CLI system
  * @param  None
  * @retval None
  */
cli_menu_t ** Menu_Initialize ( void )
{
  menuEntries[CLI_MENU_MAIN]     = &menuMain;
  menuEntries[CLI_MENU_BATTERY]  = &menuBattery;
  menuEntries[CLI_MENU_STATUS]   = &menuStatus;
  menuEntries[CLI_MENU_ASK]      = &menuAsk;
  menuEntries[CLI_MENU_CONTROL]  = &menuControl;
  menuEntries[CLI_MENU_DAKOTA]   = &menuDakota;
  menuEntries[CLI_MENU_POWER]    = &menuPower;
  menuEntries[CLI_MENU_HELP]     = &menuHelp;
  menuEntries[CLI_MENU_INPUT]    = &menuInput;

  return menuEntries;
}


/**
  * @brief  UnInitialize the CLI system
  * @param  None
  * @retval None
  */
void CLI_Uninit ( void )
{

}


/************************************************************************
 *************************** CLI Menu Functions *************************
 ************************************************************************/

static void No_Action ( uint8_t * d )
{

}



/************************************************************************
 ************************ System State Functions ************************
 ************************************************************************/

static void CLI_SystemSleep ( uint8_t * d )
{
  UNUSED(d);
  LOGI("[CLI] Enter Sleep State\r\n");
  System_Transition_State( SYS_STATE_SLEEP );
}

static void CLI_SystemIdle ( uint8_t * d )
{
  UNUSED(d);
    LOGI("[CLI] Enter Idle State\r\n");
    System_Transition_State( SYS_STATE_IDLE );
}

static void CLI_SystemWifi ( uint8_t * d )
{
  UNUSED(d);
    LOGI("[CLI] Enter WiFi State\r\n");
    System_Transition_State( SYS_STATE_WIFI );
}

static void CLI_SystemSatcom ( uint8_t * d )
{
  UNUSED(d);
    LOGI("[CLI] Enter Satcom State\r\n");
    System_Transition_State( SYS_STATE_SATCOM );
}

static void CLI_SystemBootload ( uint8_t * d )
{
  UNUSED(d);
    LOGI("[CLI] Enter Bootloader\r\n");
    System_Flag_Set(SYS_FLAG_ENTER_BOOTLOADER);
}


/************************************************************************
 ************************** Power Rail Functions ************************
 ************************************************************************/

static void vWifi_Set ( uint8_t * d )
{
  if ( *d == 1 )
  {
    LOGI("V Wifi Enabled\r\n");
    HAL_GPIO_WritePin(VWIFI_EN_PORT, VWIFI_EN_PIN, VWIFI_STATE_ON);
  }
  else
  {
    LOGI("V Wifi Disabled\r\n");
    HAL_GPIO_WritePin(VWIFI_EN_PORT, VWIFI_EN_PIN, VWIFI_STATE_OFF);
  }
}

static void vSatcom_Set ( uint8_t * d )
{
  if ( *d == 1 )
  {
    LOGI("V Satcom Enabled\r\n");
    HAL_GPIO_WritePin( VAB_EN_PORT, VAB_EN_PIN, VAB_EN_ON);
  }
  else
  {
    LOGI("V Satcom Disabled\r\n");
    HAL_GPIO_WritePin( VAB_EN_PORT, VAB_EN_PIN, VAB_EN_OFF);
  }
}

static void vUSB_Set ( uint8_t * d )
{
  if ( *d == 1 )
  {
    LOGI("V USB Enabled\r\n");
    HAL_GPIO_WritePin( VUSB_EN_PORT, VUSB_EN_PIN, VUSB_STATE_ON);
  }
  else
  {
    LOGI("V USB Disbled\r\n");
    HAL_GPIO_WritePin( VUSB_EN_PORT, VUSB_EN_PIN, VUSB_STATE_OFF);
  }
}

static void vBoost_Set ( uint8_t * d )
{
  if ( *d == 1 )
  {
    LOGI("V Boost Enabled\r\n");
    HAL_GPIO_WritePin( VBOOST_EN_PORT, VBOOST_EN_PIN, VBOOST_STATE_ON);
  }
  else
  {
    LOGI("V Boost Disbled\r\n");
    HAL_GPIO_WritePin( VBOOST_EN_PORT, VBOOST_EN_PIN, VBOOST_STATE_OFF);
  }
}



/************************************************************************
 ************************* Dakota Menu Functions ************************
 ************************************************************************/

static void Dakota_Reset_Toggle ( uint8_t * d )
{
  if (*d == 0 )
  {
    LOGI("CLI FLAG: Dakota RESET = Low\r\n");
    HAL_GPIO_WritePin(HKC_DAK_RST_PORT, HKC_DAK_RST_PIN, GPIO_PIN_RESET);
  }
  else
  {
    LOGI("CLI FLAG: Dakota RESET = High\r\n");
    HAL_GPIO_WritePin(HKC_DAK_RST_PORT, HKC_DAK_RST_PIN, GPIO_PIN_SET);
  }
}

static void Dakota_Tx_Push ( uint8_t * d )
{
  LOGD("CLI FLAG: IPQ Tx Push\r\n");
  Dakota_Send_Buffer(d, 5);
}


static void Dakota_Queue_Reset ( uint8_t * d )
{
  UNUSED(d);
  LOGD("[CLI] IPQ Queue Reset\r\n");
  Dakota_Queue_Clear();
}


/************************************************************************
 ************************ Battery Menu Functions ************************
 ************************************************************************/

static void Battery_1_Toggle ( uint8_t * d )
{
  if (*d == 0 )
  {
    LOGI("CLI FLAG: Battery1 = OFF\r\n");
    Battery_Off(BATT_B1);
  }
  else
  {
    LOGI("CLI FLAG: Battery1 = ON\r\n");
    Battery_On(BATT_B1);
  }
}

static void Battery_2_Toggle ( uint8_t * d )
{
  if (*d == 0 )
  {
    LOGI("CLI FLAG: Battery1 = OFF\r\n");
    Battery_Off(BATT_B1);
  }
  else
  {
    LOGI("CLI FLAG: Battery1 = ON\r\n");
    Battery_On(BATT_B1);
  }
}

static void Battery_Ext_Toggle ( uint8_t * d )
{
  if (*d == 0 )
  {
    LOGI("CLI FLAG: Battery Ext = OFF\r\n");
    Battery_External_Off();
  }
  else
  {
    LOGI("CLI FLAG: Battery Ext = ON\r\n");
    Battery_External_On();
  }
}

static void Charge_Select ( uint8_t * d )
{
  LOGI("CLI FLAG: Select Batt %d for charge\r\n", *d + 1);
  LT3650_Select(*d);
}

static void Charge_Set ( uint8_t * d )
{
  if (*d == 0 )
  {
    LOGI("CLI FLAG: Charge OFF\r\n");
    LT3650_Disable();
  }
  else
  {
    LOGI("CLI FLAG: Charge ON\r\n");
    LT3650_Enable();
  }
}

static void Charge_LB_Set ( uint8_t * d )
{
  if (*d == 0 )
  {
    LOGI("CLI FLAG: Load Balancing OFF\r\n");
    LT3650_LB_Set(false);
  }
  else
  {
    LOGI("CLI FLAG: Load Balancing ON\r\n");
    LT3650_LB_Set(true);
  }
}

static void FuelGauge_Info ( uint8_t * d )
{
  UNUSED(d);
  LOGD("CLI FLAG: Fuel gauge info\r\n");
  Fuel_Gauge_Print();
}

static void FuelGauge_Refresh ( uint8_t * d )
{
  UNUSED(d);
  LOGD("CLI FLAG: Fuel gauge refresh\r\n");
  Fuel_Gauge_Read_Data(BATT_B1);
  Fuel_Gauge_Read_Data(BATT_B2);
}


static void FuelGauge_StdRead ( uint8_t * d )
{
  LOGD("CLI FLAG: Fuel gauge STD read\r\n");
  uint16_t addr = (uint16_t)*(d+1);
  Fuel_Gauge_Read ( (batt_cnt_t)*d, *(d+2), addr );
}


static void FuelGauge_MacRead ( uint8_t * d )
{
  LOGD("CLI FLAG: Fuel gauge MAC read\r\n");
  uint16_t addr = ( *(d+1) << 8 ) | *(d+2);
  Fuel_Gauge_Read_MAC ( (batt_cnt_t)*d, *(d+3), addr );
}

static void FuelGauge_MacWrite ( uint8_t * d )
{
  LOGD("CLI FLAG: Fuel gauge MAC write\r\n");
  uint16_t addr = ( *(d+1) << 8 ) | *(d+2);
  Fuel_Gauge_Write_MAC ( (batt_cnt_t)*d, *(d+3), addr, (d+4) );
}

static void Battery_Override ( uint8_t * d )
{
  UNUSED(d);
  LOGD("CLI FLAG: Battery override enabled\r\n");
  Power_Disable_Module();
}


/************************************************************************
 ****************************** ASK Functions ***************************
 ************************************************************************/

static void Ask_Set_Power ( uint8_t * d )
{
  if (*d == 1 )
  {
    LOGD("[CLI]: ASK Enable\r\n");
    HAL_GPIO_WritePin( VAB_EN_PORT,   VAB_EN_PIN, VAB_EN_ON);
    Ask_Power_Set(ASK_ON);
    Ask_Tx_Enable();
  }
  else
  {
    LOGD("[CLI]: ASK Disable\r\n");
    HAL_GPIO_WritePin( VAB_EN_PORT,   VAB_EN_PIN, VAB_EN_OFF);
    Ask_Power_Set(ASK_OFF);
    Ask_Tx_Disable();
  }
}

static void Ask_Cmd_Debug ( uint8_t * d )
{
  UNUSED(d);
  LOGD("CLI FLAG: ASK Debug\r\n");
  Ask_Queue_Cmd(ASK_CMD_DEBUG, 0, NULL);
}


/************************************************************************
 ************************ Status Display Functions **********************
 ************************************************************************/

/**
  * @brief  Display system status menu
  * @param  None
  * @retval None
  */
void CLI_Display_Status ( void )
{
  RTC_TimeTypeDef sTime = Sys_Time_Get();

  LOGC("\r*** (%s, %s, %s) @%02d:%02d:%02d (%s) ***\r\n", SysStateStrs[smLast], SysStateStrs[smState], SysStateStrs[smNext], sTime.Hours, sTime.Minutes, sTime.Seconds, System_Get_Firmware());
  LOGC("  Dakota Status     = %s\r\n", dakStatusStrs[Dakota_Status_Get()]);
  LOGC("  Satcom Status     = %s (%d)\r\n", satStatusStrs[Satcom_Status_Get()], HAL_GPIO_ReadPin(AB_STATUS_PORT, AB_STATUS_PIN));
  CLI_Print_Battery_Gauge();
  LOGC("  Batt 1 Info       = OV: %d, LB: %d, STAT: %s\r\n", Battery_OV_Get(BATT_B1), Battery_LB_Okay_Get(BATT_B1), BattStatusStrs[Battery_Status_Get(BATT_B1)]);
  LOGC("  Batt 2 Info       = OV: %d, LB: %d, STAT: %s\r\n", Battery_OV_Get(BATT_B2), Battery_LB_Okay_Get(BATT_B2), BattStatusStrs[Battery_Status_Get(BATT_B2)]);
  LOGC("  Fuel Gauge 1 Info = FG: (%.4d/%.4d) I=%.4d, V=%.4d, T=%.2d\r\n", Fuel_Gauge_Get_Remaining_Capacity(BATT_B1), Fuel_Gauge_Get_Full_Capacity(BATT_B1), Fuel_Gauge_Get_Current(BATT_B1), Fuel_Gauge_Get_Voltage(BATT_B1), Fuel_Gauge_Get_Temperature(BATT_B1));
  LOGC("  Fuel Gauge 2 Info = FG: (%.4d/%.4d) I=%.4d, V=%.4d, T=%.2d\r\n", Fuel_Gauge_Get_Remaining_Capacity(BATT_B2), Fuel_Gauge_Get_Full_Capacity(BATT_B2), Fuel_Gauge_Get_Current(BATT_B2), Fuel_Gauge_Get_Voltage(BATT_B2), Fuel_Gauge_Get_Temperature(BATT_B2));
  LOGC("  Batt Pins 1       = OV: %d, En: %d\r\n", HAL_GPIO_ReadPin(BATT1_OV_PORT, BATT1_OV_PIN), HAL_GPIO_ReadPin(BATT1_SEL_PORT, BATT1_SEL_PIN));
  LOGC("  Batt Pins 2       = OV: %d, En: %d\r\n", HAL_GPIO_ReadPin(BATT2_OV_PORT, BATT2_OV_PIN), HAL_GPIO_ReadPin(BATT2_SEL_PORT, BATT2_SEL_PIN));
  LOGC("  Charger Info      = STATE: %s, NEXT: %s, MODE: %s\r\n", Power_Get_Charger_State() == CHRG_STATE_DISABLED? "Disabled":"Enabled", Power_Get_Charger_Next_State() == CHRG_STATE_DISABLED? "Disabled":"Enabled", chrgModeStr[(uint8_t)Charger_LB_Get()]);
  LOGC("  Charger Pins      = AC: %d, EN: %s, SEL: %s, STAT: %s, FLT: %s\r\n", Charger_AC_Get(), Charger_Enable_Get()? "Yes":"No", battSelStr[Charger_Slot_Get()], Charger_Status_Get()? "On":"Off", Charger_Fault_Get()? "Yes":"No");
  LOGC("  ADC               = [  B1,     B2,   T1,  T2,  T3,  | 12Ext,  ACDC,    SYS,    T,    Vref]\r\n");
  //LOGC("  ADC Raw           = [%d, %d, %d, %d, %d, | %d, %d, %d, %d, %d]\r\n", Adc_Get_Raw(ADC1_1_VBATT1_INDEX), Adc_Get_Raw(ADC1_2_VBATT2_INDEX), Adc_Get_Raw(ADC1_4_TEMP1_INDEX), Adc_Get_Raw(ADC1_5_TEMP2_INDEX), Adc_Get_Raw(ADC1_6_TEMP3_INDEX), Adc_Get_Raw(ADC1_7_V12EXT_INDEX), Adc_Get_Raw(ADC1_8_VACDC_INDEX), Adc_Get_Raw(ADC1_15_VSYS_INDEX), Adc_Get_Raw(ADC1_TEMP_INDEX), Adc_Get_Raw(ADC1_VREF_INDEX));
  LOGC("  ADC Converted     = [%dmV, %dmV, %dC, %dC, %dC, | %dmV, %dmV, %dmV, %dC, %dmV]\r\n", Adc_Voltage_Convert(ADC1_1_VBATT1_INDEX), Adc_Voltage_Convert(ADC1_2_VBATT2_INDEX), Adc_Get_Temp(ADC1_4_TEMP1_INDEX), Adc_Get_Temp(ADC1_5_TEMP2_INDEX), Adc_Get_Temp(ADC1_6_TEMP3_INDEX), Adc_Voltage_Convert(ADC1_7_V12EXT_INDEX), Adc_Voltage_Convert(ADC1_8_VACDC_INDEX), Adc_Voltage_Convert(ADC1_15_VSYS_INDEX), Adc_Get_Temp(ADC1_TEMP_INDEX), Adc_Voltage_Convert(ADC1_VREF_INDEX));
  LOGC("\r\n");
  LOGC("Press any navigation key to return.\r\n");
}

/**
  * @brief  Display system help s
  * @param  None
  * @retval None
  */
static void CLI_Display_Help (void)
{
  LOGC("\r*** CLI Help ***\r\n");
  LOGC("  Space        : Toggle Log Pause\r\n");
  LOGC("  CTRL-C       : Clear Log\r\n");
  LOGC("  Arrow Keys   : Navigate Menu\r\n");
  LOGC("  Enter Key    : Accept input or selection\r\n");
  LOGC("  + / - Key    : Adjust log level\r\n");
  LOGC("  \r\nButons:\r\n");
  LOGC("  Power Button :\r\n");
  LOGC("    Short  (<0.5s) : None\r\n");
  LOGC("    Medium (<3s)   : Dakota programming mode\r\n");
  LOGC("    Long   (>3s)   : Boot / Power down system\r\n");
  LOGC("  SOS Button :\r\n");
  LOGC("    Short  (<0.5s) : Toggle vBoost\r\n");
  LOGC("    Medium (<3s)   : None\r\n");
  LOGC("    Long   (>3s)   : Send SOS alert\r\n");
  LOGC("  \r\n");
  LOGC("  \r\nLEDs:\r\n");
  LOGC("  Power LED :\r\n");
  LOGC("    Green : Solid   - System On\r\n");
  LOGC("          : Blink 3 - Transitioning States\r\n");
  LOGC("          : Blink 1 - Charging only\r\n");
  LOGC("  SOS LED :\r\n");
  LOGC("    Green : N/A\r\n");
  LOGC("    Red   : N/A\r\n");
  LOGC("  BOTH LED Flashing Red: System Failure. Please copy log for Dan\r\n");
  LOGC("\r\n");
  LOGC("Press any navigation key to return.\r\n");
}


/**
  * @brief   Display battery gauge visually
  * @details [++++_}
  * @param   None
  * @retval  None
  */
static void CLI_Print_Battery_Gauge ( void )
{
  char batteryStr1[12] = {'\0'};
  char batteryStr2[12] = {'\0'};
  char * pBatt[2] = { batteryStr1, batteryStr2 };

  batt_cnt_t bb = BATT_B1;
  
  for (bb = BATT_B1; bb <= BATT_B2; bb++ )
  {
    if (bb == (batt_cnt_t)Charger_Slot_Get() && Charger_Charging_Get() && Charger_Enable_Get() && Battery_Is_Enabled(bb))
    {
      (*pBatt[bb]++) = 'B';
    }
    else if (bb == (batt_cnt_t)Charger_Slot_Get() && Charger_Charging_Get() && Charger_Enable_Get())
    {
      (*pBatt[bb]++) = 'C';
    }
    else if (Battery_Is_Enabled(bb))
    {
      (*pBatt[bb]++) = 'D';
    }
    else
    {
      (*pBatt[bb]++) = '_';
    }

    sprintf(pBatt[bb], "[ %.2d%% } ", Battery_Percent_Get(bb) );
  }

  LOGC("  Battery Levels    = %s %s\r\n", batteryStr1, batteryStr2);
}

