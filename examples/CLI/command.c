/**
 * @file command.c
 * @author Daniel Bujak
 * @date August 28, 2018
 * @brief Commanding interface module
 *
 */

#include "battery_test.h"
#include "command.h"
#include "calm.h"

void Command_Handler ( uint8_t * data, uint8_t len)
{
  cmd_test_t cmd = (cmd_test_t) * (data+1);
  switch ( cmd )
  {
    case CMD_TEST_SET_BATT:
      LOGI("Test batt command received\r\n");
      BattTest_Update(0, data);
      break;

    default:
      Calm_Print_Buffer(LOG_INFO, data, len, "Received command: ");
      break; 
  }
}

