

  calmInit.period    = 0;
  calmInit.cmdFunc   = &Command_Handler;
  calmInit.cliEnable = true;
  calmInit.menu      = Menu_Initialize();
  Calm_Initialize(calmInit);

  Calm_Register_Key(0x46, &Fuel_Gauge_Print);   /* 'F' */
  Calm_Register_Key(0x42, &BattTest_Print_All); /* 'B' */
  
  