/**
 * @file strings.c
 * @author Daniel Bujak
 * @date August 23, 2018
 * @brief Strings used for readability
 *
 */


/* Index strings for logging readability */
const char SysStateStrs   [4][10]  = { {"Sleep"}, {"Idle"}, {"WiFi"}, {"Satcom"} };
const char ChrgStateStrs  [2][15]  = { {"Not Charging"}, {"Charging"} };
const char ChrgFlagStrs   [12][15] = { {"NONE"}, {"Chrg Conn"}, {"Chrg Disc"}, {"Chrg Compl"}, {"Chrg Flt Set"}, {"Batt Flt Set"}, {"Batt Flt Clr"}, {"Temp Flt Set"}, {"Temp Flt Clr"}, {"LB Reached"}, {"80 Reached"}, {"Chrg Compl"} };
const char ChrgErrorStrs  [4][12]  = { {"Okay"}, {"No Charger"}, {"Fault Count"}, {"Temp Error"} };
const char BattStateStrs  [4][10]  = { {"Disch None"}, {"Disch 1"}, {"Disch 2"}, {"Disch All"} };
const char BattStatusStrs [4][10]  = { {"Okay"}, {"Low V"}, {"High Z"} };
const char chrgErrorStr   [5][15]  = { {"Success"}, {"No ACDC"}, {"Fault Count"}, {"Temperature"}, {"Batt Error"} };
const char battSelStr     [5][10]  = { {"Batt 1"}, {"Batt 2"}, {"Batt Ext"}, {"Batt All"}, {"Batt None"} };
const char chrgModeStr    [2][15]  = { {"Load Bal Dis"}, {"Load Bal En"} };
const char chrgSelStr     [3][10]  = { {"None"}, {"Batt 1"}, {"Batt 2"} };
const char acPresStrStr   [2][5]   = { {"Conn."}, {"Disc."} };
const char i2cStateStrings[4][10]  = { {"NONE"}, {"IDLE"}, {"RX"}, {"TX"} };
const char dakStatusStrs  [8][14]  = { {"OFF"}, {"POWERING"}, {"BOOTING"}, {"NAND MODE"}, {"NOR MODE"}, {"ONLINE"}, {"SHUTTING DOWN"}, {"BAD STATE"} };
const char satStatusStrs  [6][14]  = { {"OFF"}, {"POWERING"}, {"BOOTING"}, {"ONLINE"}, {"SHUTTING DOWN"}, {"BAD STATE"} };
const char chrgEventStrs  [7][10]  = { {"Fault"}, {"Status"}, {"Complete"}, {"Temp Flt"}, {"Temp Clr"}, {"AC Conn"}, {"AC Disc"} };
const char battEventStrs  [13][14] = { {"OV Set"}, {"OV Clear"}, {"Temp Err Set"}, {"Temp Err Clr"}, {"Low V"}, {"All Low V"}, {"All Critical"}, {"LB Reached"}, {"20 Reached"}, {"40 Reached"}, {"60 Reached"}, {"80 Reached"}, {"100 Reached"}};

