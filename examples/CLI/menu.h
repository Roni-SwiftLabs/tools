/**
 * @file menu.h
 * @author Daniel Bujak
 * @date August 23, 2018
 * @brief Command line interface menu definitions
 *
 */

/* Define to prevent recursive inclusion -------------------------------------*/
#ifndef __MENU_H
#define __MENU_H

#ifdef __cplusplus
 extern "C" {
#endif

#include <stdint.h>
#include <stdbool.h>
#include "cli.h"

/**
 * @brief Command line interface menus
 */
typedef enum
{
  CLI_MENU_MAIN = 0,                    /**< Main menu */
  CLI_MENU_BATTERY,                     /**< Battery Control Menu */
  CLI_MENU_STATUS,                      /**< System status menu */
  CLI_MENU_CONTROL,                     /**< System state control menu */
  CLI_MENU_POWER,                       /**< Power rail menu */
  CLI_MENU_ASK,                         /**< ASK command menu */
  CLI_MENU_DAKOTA,                      /**< Dakota menu */
  CLI_MENU_HELP,                        /**< System help menu */
  CLI_MENU_INPUT,                       /**< User input screen */
  CLI_MENU_COUNT
} menu_list_t;


/* Function Prototypes */
void            CLI_Display_Status ( void );
cli_menu_t **   Menu_Initialize    ( void );
void            CLI_Uninit         ( void );


#ifdef __cplusplus
}
#endif
#endif /* __MENU_H */
