/**
 * @file strings.h
 * @author Daniel Bujak
 * @date August 23, 2018
 * @brief Strings used for readability
 *
 */

/* Define to prevent recursive inclusion -------------------------------------*/
#ifndef __STRINGS_H
#define __STRINGS_H

#ifdef __cplusplus
 extern "C" {
#endif

/* Exported strings */
extern const char SysStateStrs    [4] [10];
extern const char ChrgStateStrs   [2] [15];
extern const char ChrgFlagStrs    [12][15];
extern const char ChrgErrorStrs   [4] [12];
extern const char BattStateStrs   [4] [10];
extern const char BattStatusStrs  [4] [10];
extern const char chrgErrorStr    [5] [15];
extern const char battSelStr      [5] [10];
extern const char chrgModeStr     [2] [15];
extern const char chrgSelStr      [3] [10];
extern const char acPresStrStr    [2] [5];
extern const char i2cStateStrings [4] [10];
extern const char dakStatusStrs   [8] [14];
extern const char satStatusStrs   [6] [14];
extern const char chrgEventStrs   [7] [10];
extern const char battEventStrs   [13][14];

#ifdef __cplusplus
}
#endif
#endif /* __STRINGS_H */


